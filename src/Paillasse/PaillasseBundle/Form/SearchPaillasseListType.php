<?php
/**
 * Created by PhpStorm.
 * User: deshiloh
 * Date: 29/08/2017
 * Time: 10:30
 */

namespace Paillasse\PaillasseBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;

class SearchPaillasseListType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("dateDebut", DateType::class, [
                "label" => "Recherche par dates",
                "widget" => "single_text",
                "html5" => false,
                "attr" => [
                    "class" => "rangePicker"
                ]
            ])
        ;
    }
}