<?php
/**
 * Created by PhpStorm.
 * User: deshiloh
 * Date: 28/08/2017
 * Time: 12:00
 */

namespace Paillasse\PaillasseBundle\Form;


use Doctrine\Common\Persistence\ObjectManager;
use Paillasse\PaillasseBundle\Form\DataTransformer\MoleculeEchantillonToStringTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class PaillasseAnalyseType extends AbstractType
{
    private $em;

    public function __construct(ObjectManager $manager)
    {
        $this->em = $manager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("moleculesEchantillon", HiddenType::class, [
                "invalid_message" => "Vous n'avez pas enregistré au moins une molécule.",
                "attr" => [
                    "class" => "molecule_datas"
                ]
            ])
            ->add("Submit", SubmitType::class, [
                "label" => "Étape suivante",
                "attr" => [
                    "class" => "btn-primary btn-sm"
                ]
            ])
        ;

        $builder->get("moleculesEchantillon")->addModelTransformer(new MoleculeEchantillonToStringTransformer($this->em));
    }
}