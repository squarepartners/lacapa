<?php

namespace Paillasse\PaillasseBundle\Service;

use Spipu\Html2Pdf\Html2Pdf;

class PDF
{
    private $pdf;

    public function create()
    {
        $this->pdf = new Html2Pdf('P', 'A4', 'fr');
        $this->pdf->setTestIsImage(false);
    }

    public function generatePDF($template, $name)
    {
        $this->pdf->pdf->IncludeJS("print(true);");
        $this->pdf->writeHTML($template);
        return $this->pdf->Output($name.'.pdf');
    }
}