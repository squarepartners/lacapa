<?php

namespace Paillasse\PaillasseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Echantillon\EchantillonBundle\Entity\Echantillon;

/**
 * Commentaire
 *
 * @ORM\Table(name="commentaire")
 * @ORM\Entity(repositoryClass="Paillasse\PaillasseBundle\Repository\CommentaireRepository")
 */
class Commentaire
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="commentaire", type="blob", nullable=true)
     */
    private $commentaire;

    /**
     * @var Echantillon
     * @ORM\ManyToOne(targetEntity="Echantillon\EchantillonBundle\Entity\Echantillon")
     */
    private $echantillon;

    /**
     * @var Paillasse
     * @ORM\ManyToOne(targetEntity="Paillasse\PaillasseBundle\Entity\Paillasse")
     */
    private $paillasse;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set commentaire
     *
     * @param string $commentaire
     *
     * @return Commentaire
     */
    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    /**
     * Get commentaire
     *
     * @return string
     */
    public function getCommentaire()
    {
        if ($this->commentaire != ""){
            return stream_get_contents($this->commentaire);
        }else {
            return '';
        }
    }

    /**
     * Set echantillon
     *
     * @param \Echantillon\EchantillonBundle\Entity\Echantillon $echantillon
     *
     * @return Commentaire
     */
    public function setEchantillon(\Echantillon\EchantillonBundle\Entity\Echantillon $echantillon = null)
    {
        $this->echantillon = $echantillon;

        return $this;
    }

    /**
     * Get echantillon
     *
     * @return \Echantillon\EchantillonBundle\Entity\Echantillon
     */
    public function getEchantillon()
    {
        return $this->echantillon;
    }

    /**
     * Set paillasse
     *
     * @param \Paillasse\PaillasseBundle\Entity\Paillasse $paillasse
     *
     * @return Commentaire
     */
    public function setPaillasse(\Paillasse\PaillasseBundle\Entity\Paillasse $paillasse = null)
    {
        $this->paillasse = $paillasse;

        return $this;
    }

    /**
     * Get paillasse
     *
     * @return \Paillasse\PaillasseBundle\Entity\Paillasse
     */
    public function getPaillasse()
    {
        return $this->paillasse;
    }
}
