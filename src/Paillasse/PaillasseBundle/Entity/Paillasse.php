<?php

namespace Paillasse\PaillasseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Utilisateurs\UtilisateursBundle\Entity\Utilisateurs;

/**
 * Paillasse
 *
 * @ORM\Table(name="paillasse")
 * @ORM\Entity(repositoryClass="Paillasse\PaillasseBundle\Repository\PaillasseRepository")
 */
class Paillasse
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateCreation", type="datetime")
     */
    private $dateCreation;

    /**
     * @var int
     * @ORM\Column(type="integer", length=3, name="EmptyFields", nullable=true)
     */
    private $nbEmptyField;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateModification", type="datetime", nullable=true)
     */
    private $dateModification;

    /**
     * @ORM\ManyToOne(targetEntity="Utilisateurs\UtilisateursBundle\Entity\Utilisateurs")
     */
    private $utilisateur;

    /**
     * @var bool
     * @ORM\Column(type="boolean", name="actif")
     */
    private $actif;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     *
     * @return Paillasse
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set dateModification
     *
     * @param \DateTime $dateModification
     *
     * @return Paillasse
     */
    public function setDateModification($dateModification)
    {
        $this->dateModification = $dateModification;

        return $this;
    }

    /**
     * Get dateModification
     *
     * @return \DateTime
     */
    public function getDateModification()
    {
        return $this->dateModification;
    }


    /**
     * Set utilisateur
     *
     * @param \Utilisateurs\UtilisateursBundle\Entity\Utilisateurs $utilisateur
     *
     * @return Paillasse
     */
    public function setUtilisateur(\Utilisateurs\UtilisateursBundle\Entity\Utilisateurs $utilisateur = null)
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    /**
     * Get utilisateur
     *
     * @return \Utilisateurs\UtilisateursBundle\Entity\Utilisateurs
     */
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }

    /**
     * Set nbEmptyField
     *
     * @param integer $nbEmptyField
     *
     * @return Paillasse
     */
    public function setNbEmptyField($nbEmptyField)
    {
        $this->nbEmptyField = $nbEmptyField;

        return $this;
    }

    /**
     * Get nbEmptyField
     *
     * @return integer
     */
    public function getNbEmptyField()
    {
        return $this->nbEmptyField;
    }

    /**
     * @return bool
     */
    public function isActif(): bool
    {
        return $this->actif;
    }

    /**
     * @param  bool  $actif
     */
    public function setActif(bool $actif): void
    {
        $this->actif = $actif;
    }
}
