<?php

namespace Paillasse\PaillasseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PaillasseMolecule
 *
 * @ORM\Table(name="paillasse_molecule")
 * @ORM\Entity(repositoryClass="Paillasse\PaillasseBundle\Repository\PaillasseMoleculeRepository")
 */
class PaillasseMolecule
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    /**
     * @ORM\ManyToOne(targetEntity="Paillasse\PaillasseBundle\Entity\Paillasse")
     */
    private $paillasse;

    /**
     * @ORM\ManyToOne(targetEntity="Echantillon\EchantillonBundle\Entity\MoleculesEchantillon", inversedBy="moleculePaillass")
     */
    private $moleculeEchantillon;

    /**
     * @ORM\ManyToOne(targetEntity="Molecules\MoleculesBundle\Entity\Methodes")
     */
    private $methode;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return PaillasseMolecule
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }


    /**
     * Set paillasse
     *
     * @param \Paillasse\PaillasseBundle\Entity\Paillasse $paillasse
     *
     * @return PaillasseMolecule
     */
    public function setPaillasse(\Paillasse\PaillasseBundle\Entity\Paillasse $paillasse = null)
    {
        $this->paillasse = $paillasse;

        return $this;
    }

    /**
     * Get paillasse
     *
     * @return \Paillasse\PaillasseBundle\Entity\Paillasse
     */
    public function getPaillasse()
    {
        return $this->paillasse;
    }

    /**
     * Set moleculeEchantillon
     *
     * @param \Echantillon\EchantillonBundle\Entity\MoleculesEchantillon $moleculeEchantillon
     *
     * @return PaillasseMolecule
     */
    public function setMoleculeEchantillon(\Echantillon\EchantillonBundle\Entity\MoleculesEchantillon $moleculeEchantillon = null)
    {
        $this->moleculeEchantillon = $moleculeEchantillon;

        return $this;
    }

    /**
     * Get moleculeEchantillon
     *
     * @return \Echantillon\EchantillonBundle\Entity\MoleculesEchantillon
     */
    public function getMoleculeEchantillon()
    {
        return $this->moleculeEchantillon;
    }

    /**
     * Set methode
     *
     * @param \Molecules\MoleculesBundle\Entity\Methodes $methode
     *
     * @return PaillasseMolecule
     */
    public function setMethode(\Molecules\MoleculesBundle\Entity\Methodes $methode = null)
    {
        $this->methode = $methode;

        return $this;
    }

    /**
     * Get methode
     *
     * @return \Molecules\MoleculesBundle\Entity\Methodes
     */
    public function getMethode()
    {
        return $this->methode;
    }
}
