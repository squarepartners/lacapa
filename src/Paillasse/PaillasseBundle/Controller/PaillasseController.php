<?php

namespace Paillasse\PaillasseBundle\Controller;

use Client\ClientBundle\Entity\Client;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Query\ResultSetMapping;
use Echantillon\EchantillonBundle\Entity\Echantillon;
use Paillasse\PaillasseBundle\Entity\Commentaire;
use Paillasse\PaillasseBundle\Entity\Paillasse;
use Paillasse\PaillasseBundle\Entity\PaillasseMolecule;
use Paillasse\PaillasseBundle\Form\PaillasseAnalyseType;
use Paillasse\PaillasseBundle\Form\SearchPaillasseListType;
use PDO;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\Request;

class PaillasseController extends Controller
{
    public function listAction($start, $end)
    {
        $em = $this->getDoctrine()->getManager();

        if($start && $end){
            $start      = new \DateTime($start);
            $end    = new \DateTime($end);
            $paillasses = $em->getRepository("PaillasseBundle:Paillasse")->getBetweenDate($start, $end);
        }else {
            $paillasses = $em->getRepository("PaillasseBundle:Paillasse")->findBy([

            ],
            [
                "dateCreation" => "DESC"
            ]);
        }

        $form = $this->createForm(SearchPaillasseListType::class);

        return $this->render("PaillasseBundle:Default:list.html.twig", [
            "paillasses" => $paillasses,
            "form" => $form->createView(),
            "startDate" => ($start) ? $start : null,
            "endDate"   => ($end) ? $end : null
        ]);
    }

    public function resetAction(Request $request)
    {
        $em     = $this->getDoctrine()->getManager();
        /** @var PDO $conn */
        $conn   = $this->getDoctrine()->getConnection();
        $search         = false;
        $echantillons   = false;
        $nbPage         = false;
        $itemPerPage    = 20;
        $currentPage    = ($request->query->get('page')) ? $request->query->get('page') : 1;

        if ($request->isMethod("GET")){
            $search = strtoupper($request->query->get("search"));

            /**
             * Requetes SQL Native
             */
            $sqlSearch = "SELECT echantillon.*, C2.Abrege 
FROM echantillon as echantillon 
LEFT JOIN molecules_echantillon on echantillon.id = molecules_echantillon.echantillon_id 
LEFT JOIN molecules on molecules_echantillon.molecules_id = molecules.id 
LEFT JOIN familles F on molecules.famille_id = F.id 
LEFT JOIN client C2 on echantillon.client_id = C2.id 
WHERE F.Nom = :famille AND molecules_echantillon.InPaillasse = 1 
GROUP BY echantillon.id 
ORDER BY echantillon.DateArriv DESC 
LIMIT ".($currentPage - 1) * $itemPerPage.", {$itemPerPage}";
            $sqlSearchCount = "SELECT * FROM echantillon as echantillon 
LEFT JOIN molecules_echantillon on echantillon.id = molecules_echantillon.echantillon_id 
LEFT JOIN molecules on molecules_echantillon.molecules_id = molecules.id 
LEFT JOIN familles F on molecules.famille_id = F.id 
WHERE F.Nom = :famille AND molecules_echantillon.InPaillasse = 1 GROUP BY echantillon.id";
            $statement = $conn->prepare($sqlSearch);
            $statement->execute([
                'famille'   => $search
            ]);
            $echantillons = $statement->fetchAll();

            // Requete pour le count
            $statement = $conn->prepare($sqlSearchCount);
            $statement->execute([
                'famille'   => $search
            ]);
            $nbEchantillons = $statement->fetchAll();

            $nbPage = ceil(count($nbEchantillons) / $itemPerPage);
        }

        // Action du reset de la fiche de paillasse
        if ($request->isMethod('POST')) {
            if ($request->request->get("echantillons")){
                $echantillonsData   = $request->request->get("echantillons");
                $search             = $request->request->get('search');


                foreach ($echantillonsData as $echantillonsDatum) {
                    // Récupération de la dernière fiche de paillasse correspondant à la recherche
                    $sqlPaillasse = "SELECT paillasse.* FROM paillasse 
JOIN paillasse_molecule pm on paillasse.id = pm.paillasse_id 
JOIN molecules_echantillon me on pm.molecule_echantillon_id = me.id 
JOIN echantillon E on me.echantillon_id = E.id JOIN molecules M on me.molecules_id = M.id 
JOIN familles F on M.famille_id = F.id
WHERE E.id = :echantillon AND F.Nom LIKE :famille AND Paillasse.actif = 1
GROUP BY paillasse.id
ORDER BY paillasse.dateCreation
                ";
                    $stmt = $conn->prepare($sqlPaillasse);
                    $stmt->execute([
                        'famille'       => '%'.$search.'%',
                        'echantillon'   => $echantillonsDatum
                    ]);
                    $paillasse = $stmt->fetch();

                }

                // Récupération des molécules échantillon pour les remettres en analyse
                $sqlME = "SELECT molecules_echantillon.id FROM molecules_echantillon 
JOIN echantillon E on molecules_echantillon.echantillon_id = E.id
JOIN molecules M on molecules_echantillon.molecules_id = M.id
JOIN familles F on M.famille_id = F.id
WHERE E.id IN (".implode(",", $echantillonsData).") AND F.nom = :famille";
                $statement = $conn->prepare($sqlME);
                $statement->execute([
                    'famille'   => $search
                ]);
                //$moleculesEchantillon = $em->getRepository("EchantillonBundle:MoleculesEchantillon")->getByFamilleInPaillasse($echantillonsData, $search);
                $moleculesEchantillon = $statement->fetchAll();

                $moleculeID = [];

                foreach ($moleculesEchantillon as $molecule){
                    $moleculeID[] = $molecule['id'];
                }

                $sql = "UPDATE molecules_echantillon SET InPaillasse = 0 WHERE id IN (".implode(",", $moleculeID).")";
                $stmt = $conn->prepare($sql);
                $stmt->execute();

                // On regarde si la fiche de paillasse est vide avant de la désactiver totalement
                $sql = "SELECT COUNT(*) as nb FROM paillasse_molecule
JOIN molecules_echantillon ON paillasse_molecule.molecule_echantillon_id = molecules_echantillon.id
WHERE paillasse_molecule.paillasse_id = :paillasse AND molecules_echantillon.InPaillasse = 1";
                $stmt = $conn->prepare($sql);
                $stmt->execute([
                    'paillasse' => $paillasse['id']
                ]);
                $res = $stmt->fetch(PDO::FETCH_ASSOC);
                if (intval($res['nb']) == 0) {
                    $stmt = $conn->prepare('UPDATE paillasse SET  actif = 0 WHERE id = :paillasse');
                    $stmt->execute([
                        'paillasse' => $paillasse['id']
                    ]);
                }


                $this->addFlash("success", "Opération effectuée");
            }
        }

        return $this->render("PaillasseBundle:Default:reset.html.twig", [
            "search"        => $search,
            "echantillons"  => $echantillons,
            'nbPage'        => ($nbPage) ? $nbPage : 0,
            'currentPage'   => $currentPage
        ]);
    }

    public function ficheAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $this->get("session")->set("paillasse", false);
        $pdf = $this->get('app.html2pdf');

        $echantillons = $em->getRepository("PaillasseBundle:PaillasseMolecule")->getEchantillonsFromPaillasse($id);

        $testArrayDatasGlobal = [];

        foreach($echantillons as $echantillon){

            $familles    = $em->getRepository("PaillasseBundle:PaillasseMolecule")->getFamillesFromPaillasse($id, $echantillon["id"]);
            $remarque = $em->getRepository("PaillasseBundle:Commentaire")->findOneBy([
               "paillasse"      => $id,
               "echantillon"    => $echantillon['id']
            ]);

            foreach ($familles as $famille){

                // $nbMoleculeForFamille           = $em->getRepository("MoleculesBundle:Molecules")->getCountForFamille($famille["nom"]);
                $MoleculesPaillasseForFamille   = $em->getRepository("PaillasseBundle:PaillasseMolecule")->getMoleculesForFamille($id, $famille["nom"], $echantillon['id']);
                $nbMoleculePaillasseForFamille  = count($MoleculesPaillasseForFamille);

                if($nbMoleculePaillasseForFamille >= 20){
                    $testArrayDatas = [
                            "client"        => $echantillon["abrege"],
                            "refLacapa"     => $echantillon["referenceLacapa"],
                            "nom"           => $echantillon["nom"],
                            "urgent"        => $echantillon['urgent'],
                            "methode"       => $echantillon["libelle"],
                            "commentaire"   => (!empty($echantillon["commentaire"])) ? stream_get_contents($echantillon["commentaire"]) : "",
                            "analyse"       => "Tout ".$famille["nom"],
                            "remarque"      => ($remarque) ? $remarque->getCommentaire() : ''
                    ];
                }else {
                    $testArrayDatas = [
                        "client"        => $echantillon["abrege"],
                        "refLacapa"     => $echantillon["referenceLacapa"],
                        "nom"           => $echantillon["nom"],
                        "methode"       => $echantillon["libelle"],
                        "commentaire"   => (!empty($echantillon["commentaire"])) ? stream_get_contents($echantillon["commentaire"]) : "",
                        "analyse"       => $MoleculesPaillasseForFamille,
                        "urgent"        => $echantillon['urgent'],
                        "remarque"      => ($remarque) ? $remarque->getCommentaire() : ''
                    ];
                }
                $testArrayDatasGlobal[] = $testArrayDatas;
            }
        }

        $paillasse = $em->getRepository("PaillasseBundle:Paillasse")->find($id);

        $template = $this->render("PaillasseBundle:Default:fiche.html.twig", [
           "paillasse"      => $paillasse,
            "echantillons"    => $testArrayDatasGlobal,
            "nbChampsVide"  => $paillasse->getNbEmptyField()
        ]);

        $name = 'paillasse'.(new \DateTime())->format('d_m_Y_h_i_s');

        $pdf->create();
        $pdf->generatePDF($template->getContent(), $name);
        return new Response("");
    }

    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $moleculesEchantillon = $em->getRepository("EchantillonBundle:MoleculesEchantillon")->findBy(["isAnalyse" => false]);
        $form = $this->createForm(PaillasseAnalyseType::class);
        $form->handleRequest($request);
        if($form->isSubmitted()){
            $pailasse = new Paillasse();
            $pailasse->setUtilisateur($this->get("security.token_storage")->getToken()->getUser());
            $pailasse->setDateCreation(new \DateTime());
            $count = 0;
            foreach ($form->getData()["moleculesEchantillon"] as $moleculeForAnalyse){
                $count ++;
                $paillasseMolecule = new PaillasseMolecule();
                $paillasseMolecule->setPaillasse($pailasse);
                $paillasseMolecule->setMoleculeEchantillon($moleculeForAnalyse);
                $paillasseMolecule->setPosition($count);
                $em->persist($paillasseMolecule);
            }
            $em->persist($pailasse);
            $em->flush();
            $this->get("app.log_manager")->creationLog("Ajout de la fiche de paillasse N° ".$pailasse->getId());
            return $this->redirectToRoute("paillasse_nextstep", ["id" => $pailasse->getId()]);
        }
        return $this->render('PaillasseBundle:Default:add.html.twig', [
            "moleculesEchantillon" => $moleculesEchantillon,
            "form" => $form->createView()
        ]);
    }


    public function stepAction(Request $request, $id)
    {
        $em             = $this->getDoctrine()->getManager();
        $paillasse      = $em->getRepository("PaillasseBundle:Paillasse")->find($id);
        $listFamilles   = [];

        $echantillons = $em->getRepository("PaillasseBundle:PaillasseMolecule")->getIdEchantillonFomPaillasse($id);

        $testArrayDatasGlobal = [];

        foreach($echantillons as $echantillon){

            $familles       = $em->getRepository("PaillasseBundle:PaillasseMolecule")->getFamillesFromPaillasse($id, $echantillon["id"]);

            foreach ($familles as $famille){
                $listFamilles[]                 = $famille["nom"];
                // $nbMoleculeForFamille           = $em->getRepository("MoleculesBundle:Molecules")->getCountForFamille($famille["nom"]);
                $MoleculesPaillasseForFamille   = $em->getRepository("PaillasseBundle:PaillasseMolecule")->getMoleculesForFamille($id, $famille["nom"], $echantillon['id']);
                $nbMoleculePaillasseForFamille  = count($MoleculesPaillasseForFamille);

                if($nbMoleculePaillasseForFamille >= 20){
                    $testArrayDatas = [
                        "client"        => $echantillon["abrege"],
                        "refLacapa"     => $echantillon["referenceLacapa"],
                        "nom"           => $echantillon["nom"],
                        "commentaire"   => (!empty($echantillon["commentaire"])) ? stream_get_contents($echantillon["commentaire"]) : "",
                        "analyse"       => "Tout ".$famille["nom"],
                        "famille"       => $famille["nom"],
                        'urgent'        => $echantillon['urgent']
                    ];
                }else {
                    $testArrayDatas = [
                        "client"        => $echantillon["abrege"],
                        "refLacapa"     => $echantillon["referenceLacapa"],
                        "nom"           => $echantillon["nom"],
                        "commentaire"   => (!empty($echantillon["commentaire"])) ? stream_get_contents($echantillon["commentaire"]) : "",
                        "analyse"       => $MoleculesPaillasseForFamille,
                        "famille"       => $famille["nom"],
                        'urgent'        => $echantillon['urgent']
                    ];
                }
                $testArrayDatasGlobal[] = $testArrayDatas;
            }
        }

        $methodesList = $em->getRepository("PaillasseBundle:PaillasseMolecule")->getMethodesByFamilles($listFamilles, $id);


        /**
         * SUBMIT FORM
         */
        if($request->getMethod() == "POST"){
            $datas = $request->request->get("analyse");

            $methode    = $em->getRepository("MoleculesBundle:Methodes")->find($datas["methodes"]);

            $count = 1;
            foreach ($datas["echantillons"] as $row){
                if (!empty($row['remarque'])){
                    $echantillon = $em->getRepository("EchantillonBundle:Echantillon")->findOneByReferenceLacapa($row['echantillon']);
                    $remarque = new Commentaire();
                    $remarque->setEchantillon($echantillon);
                    $remarque->setPaillasse($paillasse);
                    $remarque->setCommentaire($row['remarque']);
                    $em->persist($remarque);
                    $em->flush();
                }
                $molecules  = $em->getRepository("PaillasseBundle:PaillasseMolecule")->findByFamilleREF($row["nom"], $row["echantillon"], $id);
                foreach ($molecules as $molecule){
                    $molecule->setMethode($methode);
                    $molecule->setPosition($count);
                }
                $count ++;
            }

            /*if(isset($datas["famille"])){
                foreach ($datas["famille"] as $row){
                    $molecules  = $em->getRepository("PaillasseBundle:PaillasseMolecule")->findByFamilleREF($row["nom"], $row["ref"], $id);

                    foreach ($molecules as $molecule){
                        $molecule->setMethode($methode);
                    }
                }
            }

            if(isset($datas["molecule"])){
                foreach ($datas["molecule"] as $row){
                    $molecule   = $em->getRepository("PaillasseBundle:PaillasseMolecule")->find($row["id"]);
                    $molecule->setMethode($methode);
                }
            }*/

            $paillasse->setNbEmptyField($request->request->get("nbChampsVide"));

            $em->flush();

            return $this->redirectToRoute("paillasse_fiche", ["id" => $id]);
        }

        /*$moleculesPaillasse = $em->getRepository("PaillasseBundle:PaillasseMolecule")->findBy([
            "paillasse" => $id
        ]);*/

        /*$arrayFamille = ["LC", "GC", "HAP", "METAUX"];*/

        /*$familles = $em->getRepository("PaillasseBundle:PaillasseMolecule")->getFamillesFromPaillasse($id);
        $listFamilles = [];
        foreach ($familles as $famille){
            $listFamilles[] = $famille["nom"];
        }*/

        /*$collection = new ArrayCollection();
        foreach ($moleculesPaillasse as $molecule){
            if(in_array($molecule->getMoleculeEchantillon()->getMolecules()->getFamille()->getNom(), $arrayFamille)){
                if(!$this->inArrayMolecule($molecule->getMoleculeEchantillon()->getEchantillon()->getReferenceLacapa(), $molecule->getMoleculeEchantillon()->getMolecules()->getFamille()->getNom(), $collection)){
                    $data["analyse"]        = "Toutes";
                    $data["REF"]            = $molecule->getMoleculeEchantillon()->getEchantillon()->getReferenceLacapa();
                    $data["client"]         = $molecule->getMoleculeEchantillon()->getEchantillon()->getClient()->getNom();
                    $data["echantillon"]    = $molecule->getMoleculeEchantillon()->getEchantillon()->getNom();
                    $data["famille"]        = $molecule->getMoleculeEchantillon()->getMolecules()->getFamille()->getNom();
                    $data["remarque"]       = ($molecule->getMoleculeEchantillon()->getEchantillon()->getUrgent() == true) ?  "URGENT ".$molecule->getMoleculeEchantillon()->getEchantillon()->getCommentaire() :  $molecule->getMoleculeEchantillon()->getEchantillon()->getCommentaire();

                    $collection->add($data);
                }
            }else {
                $data["REF"]            = $molecule->getMoleculeEchantillon()->getEchantillon()->getReferenceLacapa();
                $data["id"]             = $molecule->getId();
                $data["analyse"]        = $molecule->getMoleculeEchantillon()->getMolecules()->getNom();
                $data["client"]         = $molecule->getMoleculeEchantillon()->getEchantillon()->getClient()->getNom();
                $data["echantillon"]    = $molecule->getMoleculeEchantillon()->getEchantillon()->getNom();
                $data["famille"]        = $molecule->getMoleculeEchantillon()->getMolecules()->getFamille()->getNom();
                $data["remarque"]       = ($molecule->getMoleculeEchantillon()->getEchantillon()->getUrgent() == true) ?  "URGENT ".$molecule->getMoleculeEchantillon()->getEchantillon()->getCommentaire() :  $molecule->getMoleculeEchantillon()->getEchantillon()->getCommentaire();

                $collection->add($data);
            }
        }*/
        return $this->render("PaillasseBundle:Default:step.html.twig", [
            "collection"    => $testArrayDatasGlobal,
            "methodesList"  => $methodesList
        ]);
    }

    public function testaddAction(Request $request, $id)
    {
        $em     = $this->getDoctrine()->getManager();
        $conn   = $this->getDoctrine()->getConnection();

        $listEchantillon = [];
        $famille         = "";

        if ($paillasse = $em->getRepository("PaillasseBundle:Paillasse")->find($id)){
            $echantillonInPaillasse = $em->getRepository("PaillasseBundle:PaillasseMolecule")->getIdEchantillonFomPaillasse($paillasse->getId());
        }else {
            $echantillonInPaillasse = [];
        }


        if ($request->isMethod("POST")){

            $search = strtoupper($request->request->get("search"));
            $datas  = $em->getRepository("EchantillonBundle:Echantillon")->getByFamilleLike($search);

            $listEchantillon = $datas;
            $famille = $search;

            if ($echantillonsDatasToInsert = $request->request->get("echantillons")){

                if ($id === false){
                    $paillasse = new Paillasse();
                    $paillasse->setUtilisateur($this->get("security.token_storage")->getToken()->getUser());
                    $paillasse->setDateCreation(new \DateTime());
                    $paillasse->setActif(true);
                    $em->persist($paillasse);
                    $em->flush();
                }else {
                    $paillasse = $em->getRepository("PaillasseBundle:Paillasse")->find($id);
                }

                $familleData = strtoupper($request->request->get("search"));

                $count  = 0;
                $date   = new \DateTime();

                foreach ($echantillonsDatasToInsert as $id){
                    /*$moleculesEchantillons = $em->getRepository("EchantillonBundle:MoleculesEchantillon")->getByFamilleAndEchantillon($id, $familleData);*/

                    $sql = "SELECT molecules_echantillon.id
                            FROM molecules_echantillon

                            INNER JOIN Molecules
                            ON Molecules.id = molecules_echantillon.molecules_id
                            
                            INNER JOIN Familles
                            ON Familles.id = Molecules.famille_id
                            
                            INNER JOIN Echantillon
                            ON Echantillon.id = molecules_echantillon.echantillon_id
                            
                            WHERE Familles.Nom = '$familleData' AND Echantillon.id = $id AND InPaillasse = 0";

                    $moleculesEchantillons = $conn->query($sql)->fetchAll();

                    $count ++;
                    foreach ($moleculesEchantillons as $molecule){

                        $sql = 'UPDATE molecules_echantillon SET inPaillasse = 1, DateAnalyse = :dateInsert WHERE id = :idMolecule';
                        $stmt = $conn->prepare($sql);
                        $stmt->bindValue('dateInsert', $date->format('Y-m-d H:i:s'));
                        $stmt->bindValue('idMolecule', $molecule['id']);
                        $stmt->execute();


                        /*$molecule->setInPaillasse(true);
                        $molecule->setDateAnalyse(new \DateTime());
                        $em->flush();*/

                        $sql = "INSERT INTO paillasse_molecule (paillasse_id, molecule_echantillon_id, position) VALUES (:paillasse, :molecule, :position)";
                        $stmt = $conn->prepare($sql);
                        $stmt->bindValue('paillasse', $paillasse->getId());
                        $stmt->bindValue('molecule', $molecule['id']);
                        $stmt->bindValue('position', $count);
                        $stmt->execute();


                        /*$paillasseMolecule = new PaillasseMolecule();
                        $paillasseMolecule->setPaillasse($paillasse);
                        $paillasseMolecule->setMoleculeEchantillon($molecule);
                        $paillasseMolecule->setPosition($count);
                        $em->persist($paillasseMolecule);
                        */
                    }
                }
                return $this->redirectToRoute("paillasse_add", [
                    'id' => $paillasse->getId()
                ]);
            }
        }

        return $this->render("PaillasseBundle:Default:test_add.html.twig", [
            "listEchantillon"           => $listEchantillon,
            "famille"                   => $famille,
            "paillasse"                 => $paillasse,
            "echantillonInPaillasse"    => $echantillonInPaillasse
        ]);
    }


    private function inArrayMolecule(string $ref,string $familleSearch , $array){
        $res = false;
        foreach ($array as $row){
            if($row["REF"] == $ref && $row["famille"] == $familleSearch){
                $res = true;
            }
        }
        return $res;
        /*$datas = array();
        foreach ($array as $row){
            $datas[] = $row["REF"];
        }
        if(in_array($ref, $datas)){
            return true;
        }else {
            return false;
        }*/
    }
}
