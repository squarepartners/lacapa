<?php

namespace Logs\LogsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class LogController extends Controller
{
    public function indexAction()
    {
        return $this->render('LogsBundle:Default:index.html.twig');
    }
}
