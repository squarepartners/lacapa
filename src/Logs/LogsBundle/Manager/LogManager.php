<?php

namespace Logs\LogsBundle\Manager;

use Doctrine\Common\Persistence\ObjectManager;
use Logs\LogsBundle\Entity\Logs;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Utilisateurs\UtilisateursBundle\Entity\Utilisateurs;

class LogManager{

    /**
     * @var ObjectManager
     */
    private $em;
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ObjectManager $em, ContainerInterface $container)
    {

        $this->em = $em;
        $this->container = $container;
    }

    /**
     * Creation d'un log
     *
     * @param Utilisateurs $utilisateurs
     * @param string $description
     */
    public function creationLog(string $description) {
        $user = $this->container->get("security.token_storage")->getToken()->getUser();
        $log = new Logs();
        $log->setDateCreation(new \DateTime());
        $log->setUtilisateurs($user);
        $log->setDescription($description);

        $this->em->persist($log);
        $this->em->flush();
    }
}