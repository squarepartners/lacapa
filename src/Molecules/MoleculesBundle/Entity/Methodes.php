<?php

namespace Molecules\MoleculesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Methodes
 *
 * @ORM\Table(name="methodes")
 * @ORM\Entity(repositoryClass="Molecules\MoleculesBundle\Repository\MethodesRepository")
 */
class Methodes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Libelle", type="string", length=255)
     */
    private $libelle;

    /**
     * @var string
     *
     * @ORM\Column(name="Value_LQ", type="decimal", precision=10, scale=3, nullable=true)
     */
    private $valueLQ;

    /**
     * @var string
     *
     * @ORM\Column(name="Value_LD", type="decimal", precision=10, scale=3, nullable=true)
     */
    private $valueLD;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateCreation", type="datetime")
     */
    private $dateCreation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateModif", type="datetime")
     */
    private $dateModif;

    /**
     * @ORM\ManyToOne(targetEntity="Molecules\MoleculesBundle\Entity\Molecules", inversedBy="methodes", cascade={"persist"})
     */
    private $molecule;

    /**
     * @var string
     * @ORM\Column(name="Unite", type="string", length=10)
     */
    private $unite;

    /**
     * @var string
     * @ORM\Column(name="Commentaire", type="blob", nullable=true)
     */
    private $commentaire;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return Methodes
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set valueLQ
     *
     * @param string $valueLQ
     *
     * @return Methodes
     */
    public function setValueLQ($valueLQ)
    {
        $this->valueLQ = $valueLQ;

        return $this;
    }

    /**
     * Get valueLQ
     *
     * @return string
     */
    public function getValueLQ()
    {
        return $this->valueLQ;
    }

    /**
     * Set valueLD
     *
     * @param string $valueLD
     *
     * @return Methodes
     */
    public function setValueLD($valueLD)
    {
        $this->valueLD = $valueLD;

        return $this;
    }

    /**
     * Get valueLD
     *
     * @return string
     */
    public function getValueLD()
    {
        return $this->valueLD;
    }

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     *
     * @return Methodes
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set dateModif
     *
     * @param \DateTime $dateModif
     *
     * @return Methodes
     */
    public function setDateModif($dateModif)
    {
        $this->dateModif = $dateModif;

        return $this;
    }

    /**
     * Get dateModif
     *
     * @return \DateTime
     */
    public function getDateModif()
    {
        return $this->dateModif;
    }

    /**
     * Set unite
     *
     * @param string $unite
     *
     * @return Methodes
     */
    public function setUnite($unite)
    {
        $this->unite = $unite;

        return $this;
    }

    /**
     * Get unite
     *
     * @return string
     */
    public function getUnite()
    {
        return $this->unite;
    }

    /**
     * Set commentaire
     *
     * @param string $commentaire
     *
     * @return Methodes
     */
    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    /**
     * Get commentaire
     *
     * @return string
     */
    public function getCommentaire()
    {
        if($this->commentaire != ""){
            return stream_get_contents($this->commentaire);
        }
    }

    /**
     * Set molecule
     *
     * @param \Molecules\MoleculesBundle\Entity\Molecules $molecule
     *
     * @return Methodes
     */
    public function setMolecule(\Molecules\MoleculesBundle\Entity\Molecules $molecule = null)
    {
        $this->molecule = $molecule;

        return $this;
    }

    /**
     * Get molecule
     *
     * @return \Molecules\MoleculesBundle\Entity\Molecules
     */
    public function getMolecule()
    {
        return $this->molecule;
    }

    /**
     * Set paillase
     *
     * @param \Paillasse\PaillasseBundle\Entity\PaillasseMolecule $paillase
     *
     * @return Methodes
     */
    public function setPaillase(\Paillasse\PaillasseBundle\Entity\PaillasseMolecule $paillase = null)
    {
        $this->paillase = $paillase;

        return $this;
    }

    /**
     * Get paillase
     *
     * @return \Paillasse\PaillasseBundle\Entity\PaillasseMolecule
     */
    public function getPaillase()
    {
        return $this->paillase;
    }

    /**
     * Set paillasse
     *
     * @param \Paillasse\PaillasseBundle\Entity\PaillasseMolecule $paillasse
     *
     * @return Methodes
     */
    public function setPaillasse(\Paillasse\PaillasseBundle\Entity\PaillasseMolecule $paillasse = null)
    {
        $this->paillasse = $paillasse;

        return $this;
    }

    /**
     * Get paillasse
     *
     * @return \Paillasse\PaillasseBundle\Entity\PaillasseMolecule
     */
    public function getPaillasse()
    {
        return $this->paillasse;
    }
}
