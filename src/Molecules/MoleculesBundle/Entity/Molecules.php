<?php

namespace Molecules\MoleculesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Molecules
 *
 * @ORM\Table(name="molecules")
 * @ORM\Entity(repositoryClass="Molecules\MoleculesBundle\Repository\MoleculesRepository")
 */
class Molecules
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var boolean
     *
     * @ORM\Column(name="actif", type="boolean")
     */
    private $actif;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateCreation", type="datetime")
     */
    private $dateCreation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateModif", type="datetime")
     */
    private $dateModif;

    /**
     * @ORM\ManyToOne(targetEntity="Molecules\MoleculesBundle\Entity\Familles")
     */
    private $famille;

    /**
     * @ORM\OneToMany(targetEntity="Molecules\MoleculesBundle\Entity\Methodes", mappedBy="molecule", cascade={"persist", "remove"})
     */
    private $methodes;

    /**
     * @var array
     *
     * @ORM\OneToMany(targetEntity="Echantillon\EchantillonBundle\Entity\MoleculesEchantillon", mappedBy="molecules", cascade={"remove"})
     */
    private $moleculesEchantillon;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Molecules
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     *
     * @return Molecules
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set dateModif
     *
     * @param \DateTime $dateModif
     *
     * @return Molecules
     */
    public function setDateModif($dateModif)
    {
        $this->dateModif = $dateModif;

        return $this;
    }

    /**
     * Get dateModif
     *
     * @return \DateTime
     */
    public function getDateModif()
    {
        return $this->dateModif;
    }

    /**
     * Set famille
     *
     * @param \Molecules\MoleculesBundle\Entity\Familles $famille
     *
     * @return Molecules
     */
    public function setFamilles(\Molecules\MoleculesBundle\Entity\Familles $famille = null)
    {
        $this->famille = $famille;

        return $this;
    }

    /**
     * Get famille
     *
     * @return \Molecules\MoleculesBundle\Entity\Familles
     */
    public function getFamilles()
    {
        return $this->famille;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->methodes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add methode
     *
     * @param \Molecules\MoleculesBundle\Entity\Methodes $methode
     *
     * @return Molecules
     */
    public function addMethode(\Molecules\MoleculesBundle\Entity\Methodes $methode)
    {
        $methode->setMolecule($this);
        $methode->setDateCreation(new \DateTime());
        $methode->setDateModif(new \DateTime());
        $this->methodes[] = $methode;
        return $this;
    }

    public function setMethodes(\Molecules\MoleculesBundle\Entity\Methodes $methodes)
    {
        foreach ($methodes as $methode) {
            $this->addMethode($methode);
        }    
    }

    /**
     * Remove methode
     *
     * @param \Molecules\MoleculesBundle\Entity\Methodes $methode
     */
    public function removeMethode(\Molecules\MoleculesBundle\Entity\Methodes $methode)
    {
        $this->methodes->removeElement($methode);
    }

    /**
     * Get methodes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMethodes()
    {
        return $this->methodes;
    }

    /**
     * Set famille
     *
     * @param \Molecules\MoleculesBundle\Entity\Familles $famille
     *
     * @return Molecules
     */
    public function setFamille(\Molecules\MoleculesBundle\Entity\Familles $famille = null)
    {
        $this->famille = $famille;

        return $this;
    }

    /**
     * Get famille
     *
     * @return \Molecules\MoleculesBundle\Entity\Familles
     */
    public function getFamille()
    {
        return $this->famille;
    }
    

    /**
     * Add moleculesEchantillon
     *
     * @param \Echantillon\EchantillonBundle\Entity\MoleculesEchantillon $moleculesEchantillon
     *
     * @return Molecules
     */
    public function addMoleculesEchantillon(\Echantillon\EchantillonBundle\Entity\MoleculesEchantillon $moleculesEchantillon)
    {
        $this->moleculesEchantillon[] = $moleculesEchantillon;

        return $this;
    }

    /**
     * Remove moleculesEchantillon
     *
     * @param \Echantillon\EchantillonBundle\Entity\MoleculesEchantillon $moleculesEchantillon
     */
    public function removeMoleculesEchantillon(\Echantillon\EchantillonBundle\Entity\MoleculesEchantillon $moleculesEchantillon)
    {
        $this->moleculesEchantillon->removeElement($moleculesEchantillon);
    }

    /**
     * Get moleculesEchantillon
     *
     * @return array
     */
    public function getMoleculesEchantillon()
    {
        return $this->moleculesEchantillon;
    }

    /**
     * Set actif.
     *
     * @param bool $actif
     *
     * @return Molecules
     */
    public function setActif($actif)
    {
        $this->actif = $actif;

        return $this;
    }

    /**
     * Get actif.
     *
     * @return bool
     */
    public function getActif()
    {
        return $this->actif;
    }
}
