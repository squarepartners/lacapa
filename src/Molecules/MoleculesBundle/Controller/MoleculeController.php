<?php

namespace Molecules\MoleculesBundle\Controller;

use Analyse\AnalyseBundle\Entity\ModeleAnalyse;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\AbstractQuery;
use Molecules\MoleculesBundle\Form\FamilleType;
use Molecules\MoleculesBundle\Form\ModeleType;
use Molecules\MoleculesBundle\Form\MoleculeType;
use Molecules\MoleculesBundle\Form\MoleculeVerifType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class MoleculeController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $molecules = $em->getRepository("MoleculesBundle:Molecules")->findAll();
        return $this->render('MoleculesBundle:Default:index.html.twig', [
            "molecules" => $molecules
        ]);
    }

    public function indexOutilVerifAction(Request $request)
    {
        $form = $this->createForm(MoleculeVerifType::class);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            foreach($form->getData() as $famille){
                return $this->redirectToRoute("molecule_outil_verif_famille", ["id" => $famille->getID()]);
            }
        }
        return $this->render("MoleculesBundle:Default:moleculeVerif.html.twig", [
            "form" => $form->createView()
        ]);
    }

    public function familleVerifAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $famille = $em->getRepository("MoleculesBundle:Familles")->find($id);
        $molecules = $em->getRepository("MoleculesBundle:Molecules")->byFamille($famille);
        return $this->render("MoleculesBundle:Default:moleculeVerifList.html.twig", [
            "molecules" => $molecules,
            "famille" => $famille
        ]);

    }

    public function formAddMoleculeAction(Request $request)
    {
        $form = $this->createForm(MoleculeType::class);
        $form->handleRequest($request);

        if($form->isSubmitted()){
            $em = $this->getDoctrine()->getManager();

            $datas = $form->getData();

            $datas->setDateCreation(new \DateTime());
            $datas->setDateModif(new \DateTime());
            $em->persist($datas);
            foreach ($datas->getMethodes() as $methodeData){
                $methodeData->setDateCreation(new \DateTime());
                $methodeData->setDateModif(new \DateTime());

                $em->persist($methodeData);
            }
            $em->flush();
            $this->addFlash("success", "La molécule a bien été enregistrée.");
        }

        return $this->render("@Molecules/Default/formAdd.html.twig", [
            "form" => $form->createView()
        ]);
    }

    public function listMethodeAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $molecule = $em->getRepository("MoleculesBundle:Molecules")->find($id);
        $methodes = $em->getRepository("MoleculesBundle:Methodes")->findBy(["molecule" => $id]);

        return $this->render("@Molecules/Default/listMethode.html.twig", [
            "methodes" => $methodes,
            "molecule" => $molecule,
        ]);
    }

    public function formUpdateAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $molecule = $em->getRepository("MoleculesBundle:Molecules")->find($id);

        $originalMethodes = new ArrayCollection();
        foreach ($molecule->getMethodes() as $methode){
            $originalMethodes->add($methode);
        }
        $form = $this->createForm(MoleculeType::class, $molecule, [
            "submit_button_label" => "Modifier"
        ]);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $datas = $form->getData();
            foreach ($originalMethodes as $methode) {
                if (false === $molecule->getMethodes()->contains($methode)) {
                   $em->remove($methode);
                }
            }
            $datas->setDateModif(new \DateTime());
            $em->flush();
            $this->addFlash("success", "La molécule a bien été modifiée.");
        }

        return $this->render("@Molecules/Default/formAdd.html.twig", [
           "form" => $form->createView(),
        ]);
    }

    public function deleteMoleculeAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $molecule = $em->getRepository("MoleculesBundle:Molecules")->find($id);
        $em->remove($molecule);
        $em->flush();
        $this->addFlash("success", "La molécule a été supprimée");
        return $this->redirectToRoute("molecules_homepage");
    }

    public function ficheAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $molecule = $em->getRepository("MoleculesBundle:Molecules")->find($id);
        if(!$molecule){
            throw $this->createNotFoundException("Molécule non trouvée.");
        }
        return $this->render("@Molecules/Default/fiche.html.twig", [
           "molecule" => $molecule,
        ]);
    }


    // FAMILLE

    public function familleFormAddAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(FamilleType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $famille = $form->getData();
            $famille->setDateCreation(new \DateTime());
            $famille->setDateModif(new \DateTime());
            $em->persist($famille);
            $em->flush();
            $this->addFlash("success", "La famille a bien été ajoutée.");
        }
        $familles = $em->getRepository("MoleculesBundle:Familles")->findAll();
        return $this->render("@Molecules/Default/famille_form_add.html.twig", [
            "form" => $form->createView(),
            "familles" => $familles,
        ]);
    }

    public function familleUpdateAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $famille = $em->getRepository("MoleculesBundle:Familles")->find($id);
        $form = $this->createForm(FamilleType::class, $famille,[
            "submit_button_label" => "Modifier",
        ]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $em->flush();
            $this->addFlash("success", "La famille a bien été modifiée.");
        }

        return $this->render("@Molecules/Default/famille_form_update.html.twig", [
            "form" => $form->createView(),
        ]);
    }

    public function familleDeleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $famille = $em->getRepository("MoleculesBundle:Familles")->find($id);
        $molecules = $em->getRepository("MoleculesBundle:Molecules")->byFamille($famille);
        foreach ($molecules as $molecule){
            $molecule->setFamilles(null);
        }
        $em->remove($famille);
        $em->flush();
        $this->addFlash("success", "La famille a bien été supprimée.");
        return $this->redirectToRoute("molecules_famille_add");
    }


    // MODELE

    public function listModeleAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $modeles = $em->getRepository("AnalyseBundle:ModeleAnalyse")->findAll();
        return $this->render("@Analyse/Default/modele_list.html.twig", [
           "modeles" => $modeles,
        ]);
    }

    public function modeleAddAction(Request $request)
    {
        $form = $this->createForm(\Analyse\AnalyseBundle\Form\ModeleType::class);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        if($form->isSubmitted() && $form->isValid()){
            $datas = $form->getData();
            $modele = new ModeleAnalyse();
            $modele->setLibelle($datas["Libelle"]);
            $modele->setMolecules($datas["Molecules"]);
            $modele->setDateCreation(new \DateTime());
            $modele->setDateModif(new \DateTime());
            $em->persist($modele);
            $em->flush();
            $this->addFlash("success", "Le modèle a bien été généré.");
        }
        return $this->render("@Analyse/Default/modele_form_add.html.twig", [
            "form" => $form->createView(),
        ]);
    }

    public function updateModeleAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $modele = $em->getRepository("AnalyseBundle:ModeleAnalyse")->find($id);
        $form = $this->createForm("Analyse\AnalyseBundle\Form\ModeleType", $modele, [
            "submit_button_label" => "Modifier"
        ]);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em->flush();
            $this->addFlash("success", "Modèle d'analyse mise à jour avec succés.");
        }

        return $this->render("@Analyse/Default/modele_form_add.html.twig", [
           "form" => $form->createView(),
        ]);

    }

    public function ficheModeleAction($id)
    {
        $molecules = array();
        $em = $this->getDoctrine()->getManager();
        $modele = $em->getRepository("AnalyseBundle:ModeleAnalyse")->find($id);
        foreach ($modele->getMolecules() as $molecule){
            $molecules[] = $em->getRepository("MoleculesBundle:Molecules")->find($molecule);
        }
        return $this->render("@Analyse/Default/modele_fiche.html.twig", [
            "modele"     => $modele,
            "molecules"  => $molecules,
        ]);
    }

    public function deleteModeleAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $modele = $em->getRepository("AnalyseBundle:ModeleAnalyse")->find($id);
        $em->remove($modele);
        $em->flush();
        $this->addFlash("success", "Le modèle d'analyse a bien été supprimé.");
        return $this->redirectToRoute("molecules_modele_list");
    }

    // JSON

    public function jsonAction()
    {
        $em = $this->getDoctrine()->getManager();
        $molecules = $em->getRepository("MoleculesBundle:Molecules")->ArrayReturn();
        return new JsonResponse($molecules);
    }

    public function familleJsonAction()
    {
        $em = $this->getDoctrine()->getManager();
        $familles = $em->getRepository("MoleculesBundle:Familles")->arrayReturn();
        return new JsonResponse($familles);
    }

    public function familleMoleculeJSONAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $moleculesInFamille = $em->getRepository("MoleculesBundle:Molecules")->findByArray($id);
        return new JsonResponse($moleculesInFamille);
    }

    public function jsonDetailAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $molecule = $em->getRepository("MoleculesBundle:Molecules")->ArrayById($id);
        return new JsonResponse($molecule);
    }

    public function modeleJSONAction()
    {
        $em = $this->getDoctrine()->getManager();
        $qb = $em->getRepository("AnalyseBundle:ModeleAnalyse")->createQueryBuilder("c")->getQuery()->getArrayResult();
        return new JsonResponse($qb);
    }

    public function modeleFicheJSONAction($id)
    {
        $datasReturn = array ();
        $em = $this->getDoctrine()->getManager();
        $modele = $em->getRepository("AnalyseBundle:ModeleAnalyse")->find($id);
        foreach ($modele->getMolecules() as $id){
            $datasReturn[] = $em->getRepository("MoleculesBundle:Molecules")
                ->createQueryBuilder("m")
                ->select("m")
                ->where("m.id = :id")
                ->andWhere('m.actif = :actif')->setParameter('actif', true)
                ->setParameter("id", $id)->getQuery()->getSingleResult(AbstractQuery::HYDRATE_ARRAY);
        }
        return new JsonResponse($datasReturn);
    }

}

