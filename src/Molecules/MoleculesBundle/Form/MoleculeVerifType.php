<?php
/**
 * Created by PhpStorm.
 * User: deshiloh
 * Date: 23/08/2017
 * Time: 16:20
 */

namespace Molecules\MoleculesBundle\Form;


use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MoleculeVerifType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("Familles", EntityType::class, [
                "class" => "Molecules\MoleculesBundle\Entity\Familles",
                "choice_label" => "Nom",
                "query_builder" => function(EntityRepository $er){
                    return $er->createQueryBuilder("f")
                        ->orderBy("f.nom", "ASC");
                },
                "label" => "Choisir la famille de molécules"
            ])
            ->add("submit", SubmitType::class, [
                "label" => "Sélectionner",
                "attr" => [
                    "class" => "btn-primary"
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
       $resolver->setDefaults([

       ]);
    }
}