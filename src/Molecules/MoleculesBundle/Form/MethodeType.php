<?php
/**
 * Created by PhpStorm.
 * User: deshiloh
 * Date: 05/07/2017
 * Time: 10:54
 */

namespace Molecules\MoleculesBundle\Form;


use Molecules\MoleculesBundle\Entity\Methodes;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MethodeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("Libelle", TextType::class,[
                "label" => "Intitulé de la méthode"
            ])
            ->add("Value_LQ", NumberType::class, [
                "scale" => 3,
                "label" => "Valeur LQ"
            ])
            ->add("Value_LD", NumberType::class, [
                "scale" => 3,
                "label" => "Valeur LD"
            ])
            ->add("Unite", ChoiceType::class, [
                "label" => "Unité de mesure",
                "choices" => [
                    "mg/kg"     => "mg/kg",
                    "µg/kg"     => "µg/kg",
                    "Bq/Kg"     => "Bq/Kg",
                    "%"         => "%"
                ]
            ])
            ->add("Commentaire", TextareaType::class, [
                "required" => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
           "data_class" => Methodes::class,
        ]);
    }
}