<?php
/**
 * Created by PhpStorm.
 * User: deshiloh
 * Date: 05/07/2017
 * Time: 10:50
 */

namespace Molecules\MoleculesBundle\Form;

use Molecules\MoleculesBundle\Entity\Molecules;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MoleculeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("Nom", TextType::class)
            ->add("familles", EntityType::class, [
                "class" => "Molecules\MoleculesBundle\Entity\Familles",
                "choice_label" => "Nom",
                "label" => "Famille"
            ])
            ->add('actif', ChoiceType::class, [
                'choices'   => [
                    'Actif'     => 1,
                    'Non Actif' => 0
                ]
            ])
            ->add("methodes", CollectionType::class, [
                "entry_type" => MethodeType::class,
                "allow_add" => true,
                "allow_delete" => true,
                "label" => false,
                'by_reference' => false,
            ])
            ->add("submit", SubmitType::class, [
                "label" => $options["submit_button_label"],
                "attr" => [
                    "class" => "btn-primary pull-right"
                ]
            ])
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            "data_class" => Molecules::class,
            "submit_button_label" => "Ajouter"
        ]);
    }
}
