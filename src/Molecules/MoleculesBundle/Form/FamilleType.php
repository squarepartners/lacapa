<?php
/**
 * Created by PhpStorm.
 * User: deshiloh
 * Date: 05/07/2017
 * Time: 10:56
 */

namespace Molecules\MoleculesBundle\Form;


use Molecules\MoleculesBundle\Entity\Familles;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FamilleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("Nom", TextType::class, [
                "attr" => [
                    "placeholder"   => "Nom de la famille"
                ]
            ])
            ->add("submit", SubmitType::class, [
                "label" => $options["submit_button_label"],
                "attr" => [
                    "class"         => "btn-primary btn-sm"
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            "data_class" => Familles::class,
            "submit_button_label" => "Ajouter"
        ]);
    }
}