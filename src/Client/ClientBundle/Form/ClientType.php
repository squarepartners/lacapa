<?php
/**
 * Created by PhpStorm.
 * User: deshiloh
 * Date: 28/06/2017
 * Time: 13:30
 */

namespace Client\ClientBundle\Form;


use Client\ClientBundle\Entity\Client;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClientType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("Nom", TextType::class)
            ->add("Abrege", TextType::class, [
                "label" => "Nom abrégé"
            ])
            ->add("NumTVA", TextType::class, [
                "label" => "Numéro TVA",
                'required' => false
            ])
            ->add("Remarque", TextareaType::class, [
                "required" => false
            ])
            ->add("Submit", SubmitType::class, [
                "label" => $options["label_submit"],
                "attr" => [
                    "class" => "btn-primary pull-right"
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            "data_class" => Client::class,
            "label_submit" => "Ajouter",
        ]);
    }
}