<?php
/**
 * Created by PhpStorm.
 * User: deshiloh
 * Date: 28/06/2017
 * Time: 16:23
 */

namespace Client\ClientBundle\Form;


use Client\ClientBundle\Entity\ClientAdresse;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClientAdresseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("Titre", TextType::class, [
                'required' => false
            ])
            ->add("ContactCL",TextType::class, [
                "label" => "Contact client"
            ])
            ->add('numTva', TextType::class, [
                'label' => 'Numéro TVA',
                'required'  => false
            ])
            ->add("Telephone", TextType::class, [
                'required'  => false
            ])
            ->add("Telephone_Autre", TextType::class, [
                "required" => false
            ])
            ->add("Rue", TextType::class)
            ->add("RueBis", TextType::class, [
                "label" => "Rue complément",
                "required" => false
            ])
            ->add("Mails", TextareaType::class, [
                "label" => "Adresses emails (séparé par des ';')",
                'required'  => false
            ])
            ->add("CodePostal", TextType::class)
            ->add("Ville", TextType::class)
            ->add("Pays", TextType::class, [
                'required' => false
            ])
            ->add("Submit", SubmitType::class,[
                "label" => $options["label_submit"],
                "attr" => [
                    "class" => "btn-primary pull-right"
                ]
            ]);

        $builder->get("Mails")->addModelTransformer(new CallbackTransformer(
            function ($mailsAsArray){
                if(!empty($mailsAsArray)){
                    return implode(";", $mailsAsArray);
                }
            },
            function ($mailsAsString){
                if (!empty($mailsAsString)) {
                    return explode(";", $mailsAsString);
                }
            }
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
           'data_class' => ClientAdresse::class,
           "label_submit" => "Ajouter"
        ]);
    }
}