<?php

namespace Client\ClientBundle\Command;

use Client\ClientBundle\Entity\Client;
use Client\ClientBundle\Entity\ClientAdresse;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class AppClientsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:clients')
            ->setDescription('Importation des clients et de leurs adresses.')
            ->addArgument('argument', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $helper = $this->getHelper("question");
        $question = new Question("<fg=red>Attention !</> ceci supprimera la liste des clients et leurs adresses actuelle, Voulez vous continuez ? [<fg=yellow>Oui</>/<fg=yellow>Non</>] ", "Non");
        $question->setAutocompleterValues(["Oui", "Non", "oui", "non"]);
        $question->setNormalizer(function($value){
           $value = strtolower($value);
           return ($value == "oui") ? true : false;
        });

        if(!$helper->ask($input, $output, $question)){
            $output->writeln("Opération annulée.");
            return false;
        }

        $em = $this->getContainer()->get("doctrine")->getManager();

        $connect = $this->getContainer()->get("doctrine")->getConnection();
        $output->writeln("Nettoyage des Clients et des leurs adresses...");

        $connect->executeQuery("SET FOREIGN_KEY_CHECKS = 0 ");
        $connect->executeQuery("DELETE FROM Client");
        $connect->executeQuery("DELETE FROM Client_Adresse");

        $connect->executeQuery("ALTER TABLE Client AUTO_INCREMENT = 0");
        $connect->executeQuery("ALTER TABLE Client_Adresse AUTO_INCREMENT = 0");

        $connect->executeQuery("SET FOREIGN_KEY_CHECKS = 1 ");

        $output->writeln("Importation des clients...");
        $clientsCSV = @file($this->getContainer()->get("kernel")->getRootDir(). DIRECTORY_SEPARATOR ."..". DIRECTORY_SEPARATOR . "var" . DIRECTORY_SEPARATOR . "client.csv");
        $clientsAdresseCSV = @file($this->getContainer()->get("kernel")->getRootDir(). DIRECTORY_SEPARATOR ."..". DIRECTORY_SEPARATOR . "var" . DIRECTORY_SEPARATOR . "client_adresses.csv");

        foreach ($clientsCSV as $row){
            $clientRow = explode(";", $row);

            $client = new Client();
            $client->setId($clientRow[0]);
            $client->setNom($clientRow[1]);
            $client->setAbrege($clientRow[2]);
            $client->setNBAnalyse(0);
            $client->setNumTVA($clientRow[13]);
            $client->setDateCreation(new \DateTime());

            $metadata = $em->getClassMetaData(get_class($client));
            $metadata->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);
            $em->persist($client);
        }

        $em->flush();

        $output->writeln("Importation des adresse...");
        foreach ($clientsAdresseCSV as $row){
            $adresseClientRow = explode(",", $row);
            $client = $em->getRepository(Client::class)->find(intval($adresseClientRow[1]));

            $adresseClient = new ClientAdresse();
            $adresseClient->setClient($client);
            $adresseClient->setTitre($adresseClientRow[2]);
            $adresseClient->setCodePostal($adresseClientRow[5]);
            $adresseClient->setContactCL($adresseClientRow[7]);
            if(!empty($adresseClientRow[10])){
                $adresseClient->setMails($adresseClientRow[10]);
            }
            if(!empty($adresseClientRow[14])){
                $adresseClient->setPays($adresseClientRow[14]);
            }
            $adresseClient->setRue($adresseClientRow[3]);
            if(!empty($adresseClientRow[4])){
                $adresseClient->setRueBis($adresseClientRow[4]);
            }
            $adresseClient->setVille($adresseClientRow[6]);
            if(!empty($adresseClientRow[9])){
                $adresseClient->setTelephone($adresseClientRow[8]);
            }
            if(!empty($adresseClientRow[9])){
                $adresseClient->setTelephoneAutre($adresseClientRow[9]);
            }
            $adresseClient->setDateCreation(new \DateTime());
            $em->persist($adresseClient);
        }
        $em->flush();
        $formatter = $this->getHelper("formatter");
        $text = ["Tout vas bien !", "Les clients et leurs adresses ont bien étés importés."];
        $blockFormatter = $formatter->formatBlock($text, "info");
        $output->writeln($blockFormatter);
    }

}
