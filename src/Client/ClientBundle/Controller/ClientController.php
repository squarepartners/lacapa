<?php

namespace Client\ClientBundle\Controller;

use Client\ClientBundle\Entity\Client;
use Client\ClientBundle\Entity\ClientAdresse;
use Client\ClientBundle\Form\ClientAdresseType;
use Client\ClientBundle\Form\ClientType;
use Logs\LogsBundle\Entity\Logs;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ClientController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $clients = $em->getRepository("ClientBundle:Client")->findAll();
        return $this->render('ClientBundle:Default:list.html.twig',[
            "clients" => $clients
        ]);
    }

    public function addFormAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(ClientType::class);
        $form->handleRequest($request);
        if($form->isSubmitted()){

            $datas = $form->getData();

            $datas->setNBAnalyse(0);
            $datas->setDateCreation(new \DateTime);
            $datas->setDateModif(new \DateTime());


            // LOG
            $user = $this->getUser();

            $log = new Logs();
            $log->setDescription("Ajout de la fiche client : ".$datas->getNom());
            $log->setUtilisateurs($user);
            $log->setDateCreation(new \DateTime());

            $em->persist($log);
            $em->persist($datas);

            $em->flush();
            $this->addFlash("success", "Le client a bien été ajouté.");

            return $this->redirectToRoute("client_fiche", [
                "id" => $datas->getId()
            ]);
        }
        return $this->render("ClientBundle:Default:ClientFormAdd.html.twig", [
            "formClient" => $form->createView()
        ]);
    }

    public function modifFormAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $client = $em->getRepository("ClientBundle:Client")->find($id);
        $form = $this->createForm(ClientType::class, $client, [
            "label_submit" => "Modifier"
        ]);

        $form->handleRequest($request);
        if($form->isSubmitted()){/*foreach ($form->getData()->getMails() as $mail){
                if(!filter_var($mail, FILTER_VALIDATE_EMAIL)){
                    $this->addFlash("danger", "Il y'a des adresses mails incorrect");
                    return $this->render("ClientBundle:Default:ClientFormModif.html.twig", [
                        "form" => $form->createView(),
                        "client" => $client
                    ]);
                }
            }*/

            $client->setDateModif(new \DateTime());

            $user = $this->getUser();

            $log = new Logs();
            $log->setDateCreation(new \DateTime());
            $log->setUtilisateurs($user);
            $log->setDescription("Modification de la fiche Client : ".$form->getData()->getNom());
            $em->persist($log);

            $em->flush();
            $this->addFlash("success", "Le client a bien été modifié.");
        }

        return $this->render("ClientBundle:Default:ClientFormModif.html.twig", [
            "form" => $form->createView(),
            "client" => $client
        ]);
    }

    public function suppAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $client = $em->getRepository("ClientBundle:Client")->find($id);

        $log = new Logs();
        $log->setDescription("Suppression de la fiche Cient : ".$client->getNom());
        $log->setDateCreation(new \DateTime());
        $log->setUtilisateurs($this->getUser());

        $em->persist($log);
        $em->remove($client);

        $em->flush();
        $this->addFlash("success", "Le client a bien été supprimé.");
        return $this->redirectToRoute("client_homepage");
    }

    public function ficheAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $client = $em->getRepository("ClientBundle:Client")->findOneBy(["id" => $id]);
        return $this->render("@Client/Default/fiche.html.twig",[
            "client" => $client
        ]);
    }


    /**
     * ADRESSE CLIENT
     */

    public function adresseAddFormAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $client = $em->getRepository("ClientBundle:Client")->find($id);

        $form = $this->createForm(ClientAdresseType::class);
        $form->handleRequest($request);

        if($form->isSubmitted()){
            $datas = $form->getData();

            foreach ($datas->getMails() as $mail){
                if(!filter_var($mail, FILTER_VALIDATE_EMAIL)){
                    $this->addFlash("danger", "Il y'a des adresses mails incorrect");
                    return $this->render("ClientBundle:Default:ClientFormAdd.html.twig", [
                        "formClient" => $form->createView()
                    ]);
                }
            }
            $datas->setClient($client);
            $datas->setMails($datas->getMails());

            $datas->setDateCreation(new \DateTime());
            $datas->setDateModif(new \DateTime());

            $em->persist($datas);
            $em->flush();

            $this->get("app.log_manager")->creationLog("Ajout d'une adresse pour le client ".$client->getNom());

            $this->addFlash("success", "L'adresse a bien été ajoutée à ".$client->getNom());
            return $this->redirectToRoute("client_fiche", ["id" => $client->getId()]);
        }

        return $this->render("@Client/Adresse/form.html.twig", [
            "form" => $form->createView(),
            "client" => $client
        ]);
    }

    public function adresseModifFormAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $adresse = $em->getRepository("ClientBundle:ClientAdresse")->find($id);
        $form = $this->createForm(ClientAdresseType::class, $adresse, [
            "label_submit" => "Modifier"
        ]);
        $form->handleRequest($request);

        if($form->isSubmitted()){
            $adresse->setDateModif(new \DateTime());
            foreach ($adresse->getMails() as $mail){
                if(!filter_var($mail, FILTER_VALIDATE_EMAIL)){
                    $this->addFlash("danger", "Il y'a des adresses mails incorrect");
                    return $this->render("@Client/Adresse/formModif.html.twig", [
                        "form" => $form->createView(),
                        "adresse" => $adresse
                    ]);
                }
            }
            $em->flush();
            $this->addFlash("success", "Modifications effectuée.");
            $this->get("app.log_manager")->creationLog("Modification de l'adresse de ".$adresse->getTitre()." pour ".$adresse->getClient()->getNom());
        }

        return $this->render("@Client/Adresse/formModif.html.twig", [
            "form" => $form->createView(),
            "adresse" => $adresse
        ]);
    }

    public function adresseSuppAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $adresse = $em->getRepository("ClientBundle:ClientAdresse")->find($id);
        $idClient = $adresse->getClient()->getId();
        $this->get("app.log_manager")->creationLog("Suppression de l'adresse ".$adresse->getTitre()." pour ".$adresse->getClient()->getNom());
        $em->remove($adresse);
        $em->flush();

        $this->addFlash("success", "Adresse supprimée");
        return $this->redirectToRoute("client_fiche", ["id" => $idClient]);
    }
}
