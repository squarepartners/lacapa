<?php

namespace Client\ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Client
 *
 * @ORM\Table(name="client")
 * @ORM\Entity(repositoryClass="Client\ClientBundle\Repository\ClientRepository")
 */
class Client
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom", type="string", length=100)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="Abrege", type="string", length=40)
     */
    private $abrege;

    /**
     * @var int
     *
     * @ORM\Column(name="NBAnalyse", type="integer")
     */
    private $nBAnalyse;

    /**
     * @var string
     *
     * @ORM\Column(name="NumTVA", type="string", length=25, nullable=true)
     */
    private $numTVA;

    /**
     * @var string
     *
     * @ORM\Column(name="Remarque", type="blob", nullable=true)
     */
    private $remarque;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateCreation", type="datetime")
     */
    private $dateCreation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateModif", type="datetime", nullable=true)
     */
    private $dateModif;

    /**
     * @var array
     * @ORM\OneToMany(targetEntity="Client\ClientBundle\Entity\ClientAdresse", mappedBy="client", cascade={"remove"})
     */
    private $adresses;

    /**
     * @var array
     * @ORM\OneToMany(targetEntity="Echantillon\EchantillonBundle\Entity\Echantillon", mappedBy="client")
     */
    private $echantillons;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function setId($id){
        $this->id = $id;

        return $this;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Client
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }
    

    /**
     * Set nBAnalyse
     *
     * @param integer $nBAnalyse
     *
     * @return Client
     */
    public function setNBAnalyse($nBAnalyse)
    {
        $this->nBAnalyse = $nBAnalyse;

        return $this;
    }

    /**
     * Get nBAnalyse
     *
     * @return int
     */
    public function getNBAnalyse()
    {
        return $this->nBAnalyse;
    }

    /**
     * Set numTVA
     *
     * @param string $numTVA
     *
     * @return Client
     */
    public function setNumTVA($numTVA)
    {
        $this->numTVA = $numTVA;

        return $this;
    }

    /**
     * Get numTVA
     *
     * @return string
     */
    public function getNumTVA()
    {
        return $this->numTVA;
    }

    /**
     * Set remarque
     *
     * @param string $remarque
     *
     * @return Client
     */
    public function setRemarque($remarque)
    {
        $this->remarque = $remarque;

        return $this;
    }

    /**
     * Get remarque
     *
     * @return string
     */
    public function getRemarque()
    {
        if($this->remarque != ""){
            return stream_get_contents($this->remarque);
        }
    }


    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     *
     * @return Client
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set dateModif
     *
     * @param \DateTime $dateModif
     *
     * @return Client
     */
    public function setDateModif($dateModif)
    {
        $this->dateModif = $dateModif;

        return $this;
    }

    /**
     * Get dateModif
     *
     * @return \DateTime
     */
    public function getDateModif()
    {
        return $this->dateModif;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->adresses = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add adress
     *
     * @param \Client\ClientBundle\Entity\ClientAdresse $adress
     *
     * @return Client
     */
    public function addAdress(\Client\ClientBundle\Entity\ClientAdresse $adress)
    {
        $this->adresses[] = $adress;

        return $this;
    }

    /**
     * Remove adress
     *
     * @param \Client\ClientBundle\Entity\ClientAdresse $adress
     */
    public function removeAdress(\Client\ClientBundle\Entity\ClientAdresse $adress)
    {
        $this->adresses->removeElement($adress);
    }

    /**
     * Get adresses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAdresses()
    {
        return $this->adresses;
    }


    /**
     * Set abrege
     *
     * @param string $abrege
     *
     * @return Client
     */
    public function setAbrege($abrege)
    {
        $this->abrege = $abrege;

        return $this;
    }

    /**
     * Get abrege
     *
     * @return string
     */
    public function getAbrege()
    {
        return $this->abrege;
    }
}
