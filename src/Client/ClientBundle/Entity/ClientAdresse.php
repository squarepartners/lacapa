<?php

namespace Client\ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClientAdresse
 *
 * @ORM\Table(name="client_adresse")
 * @ORM\Entity(repositoryClass="Client\ClientBundle\Repository\ClientAdresseRepository")
 */
class ClientAdresse
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Titre", type="string", length=255, nullable=true)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="Rue", type="string", length=255, nullable=true)
     */
    private $rue;

    /**
     * @var string
     *
     * @ORM\Column(name="RueBis", type="string", length=255, nullable=true)
     */
    private $rueBis;

    /**
     * @var string
     *
     * @ORM\Column(name="CodePostal", type="string", length=20, nullable=true)
     */
    private $codePostal;

    /**
     * @var string
     *
     * @ORM\Column(name="Ville", type="string", length=255, nullable=true)
     */
    private $ville;

    /**
     * @var string
     *
     * @ORM\Column(name="Pays", type="string", length=50, nullable=true)
     */
    private $pays;

    /**
     * @var string
     *
     * @ORM\Column(name="ContactCL", type="string", length=50, nullable=true)
     */
    private $contactCL;

    /**
     * @var string
     *
     * @ORM\Column(name="Telephone", type="string", length=15, nullable=true)
     */
    private $telephone;

    /**
     * @var array
     *
     * @ORM\Column(name="Mails", type="array")
     */
    private $mails;

    /**
     * @var string
     *
     * @ORM\Column(name="TelephoneAutre", type="string", length=15, nullable=true)
     */
    private $telephoneAutre;

    /**
     * @var string
     * @ORM\Column(name="num_tva", type="string", nullable=true)
     */
    private $num_tva;

    /**
     * @return string
     */
    public function getNumTva()
    {
        return $this->num_tva;
    }

    /**
     * @param string $num_tva
     */
    public function setNumTva(string $num_tva)
    {
        $this->num_tva = $num_tva;
    }

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateCreation", type="datetime")
     */
    private $dateCreation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateModif", type="datetime", nullable=true)
     */
    private $dateModif;

    /**
     * @ORM\ManyToOne(targetEntity="Client\ClientBundle\Entity\Client", inversedBy="adresses")
     */
    private $client;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return ClientAdresse
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set rue
     *
     * @param string $rue
     *
     * @return ClientAdresse
     */
    public function setRue($rue)
    {
        $this->rue = $rue;

        return $this;
    }

    /**
     * Get rue
     *
     * @return string
     */
    public function getRue()
    {
        return $this->rue;
    }

    /**
     * Set rueBis
     *
     * @param string $rueBis
     *
     * @return ClientAdresse
     */
    public function setRueBis($rueBis)
    {
        $this->rueBis = $rueBis;

        return $this;
    }

    /**
     * Get rueBis
     *
     * @return string
     */
    public function getRueBis()
    {
        return $this->rueBis;
    }

    /**
     * Set codePostal
     *
     * @param string $codePostal
     *
     * @return ClientAdresse
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    /**
     * Get codePostal
     *
     * @return string
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * Set ville
     *
     * @param string $ville
     *
     * @return ClientAdresse
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set pays
     *
     * @param string $pays
     *
     * @return ClientAdresse
     */
    public function setPays($pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays
     *
     * @return string
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set contactCL
     *
     * @param string $contactCL
     *
     * @return ClientAdresse
     */
    public function setContactCL($contactCL)
    {
        $this->contactCL = $contactCL;

        return $this;
    }

    /**
     * Get contactCL
     *
     * @return string
     */
    public function getContactCL()
    {
        return $this->contactCL;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     *
     * @return ClientAdresse
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set telephoneAutre
     *
     * @param string $telephoneAutre
     *
     * @return ClientAdresse
     */
    public function setTelephoneAutre($telephoneAutre)
    {
        $this->telephoneAutre = $telephoneAutre;

        return $this;
    }

    /**
     * Get telephoneAutre
     *
     * @return string
     */
    public function getTelephoneAutre()
    {
        return $this->telephoneAutre;
    }

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     *
     * @return ClientAdresse
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set dateModif
     *
     * @param \DateTime $dateModif
     *
     * @return ClientAdresse
     */
    public function setDateModif($dateModif)
    {
        $this->dateModif = $dateModif;

        return $this;
    }

    /**
     * Get dateModif
     *
     * @return \DateTime
     */
    public function getDateModif()
    {
        return $this->dateModif;
    }

    /**
     * Set client
     *
     * @param \Client\ClientBundle\Entity\Client $client
     *
     * @return ClientAdresse
     */
    public function setClient(\Client\ClientBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Client\ClientBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set mails
     *
     * @param array $mails
     *
     * @return ClientAdresse
     */
    public function setMails($mails)
    {
        $this->mails = $mails;

        return $this;
    }

    /**
     * Get mails
     *
     * @return array
     */
    public function getMails()
    {
        return $this->mails;
    }
}
