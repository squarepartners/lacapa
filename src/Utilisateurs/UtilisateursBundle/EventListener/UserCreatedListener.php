<?php
/**
 * Created by PhpStorm.
 * User: deshiloh
 * Date: 30/06/2017
 * Time: 10:21
 */

namespace Utilisateurs\UtilisateursBundle\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\FOSUserEvents;
use Logs\LogsBundle\Manager\LogManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class UserCreatedListener implements EventSubscriberInterface
{
    protected $em;
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var LogManager
     */
    private $logManager;

    public function __construct(EntityManagerInterface $entityManager, ContainerInterface $container, LogManager $logManager)
    {
        $this->em = $entityManager;
        $this->container = $container;
        $this->logManager = $logManager;
    }

    public static function getSubscribedEvents()
    {
        return array(
          FOSUserEvents::REGISTRATION_SUCCESS => ["onFormSuccess", 10]
        );
    }

    public function onFormSuccess(FormEvent $event)
    {
        $userCreated = $event->getForm()->getData();
        $this->logManager->creationLog("Creation de l'utilisateur ".$userCreated->getUserName());
    }
}