<?php

namespace Utilisateurs\UtilisateursBundle\Controller;


use PDO;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Utilisateurs\UtilisateursBundle\Entity\Utilisateurs;
use Utilisateurs\UtilisateursBundle\Form\RegistrationType;
use Utilisateurs\UtilisateursBundle\Form\UpdateUserType;
use Utilisateurs\UtilisateursBundle\Form\UtilisateurType;

class UtilisateursController extends Controller
{
    public function indexAction()
    {
        $user = $this->get("security.token_storage")->getToken()->getUser();
        $conn               = $this->getDoctrine()->getConnection();
        $perPage = 15;
        $page = (isset($_GET['page'])) ? intval($_GET['page']) : 1 ;
        $offset = ($page - 1 ) * $perPage;

        $sqlNbTotalEchantillon                  = "SELECT echantillon.id FROM echantillon INNER JOIN molecules_echantillon m ON echantillon.id = m.echantillon_id WHERE InPaillasse = 0 AND Supp = 0 GROUP BY echantillon_id";
        $sqlFamilleCount                        = "SELECT (SELECT COUNT(*) FROM molecules WHERE molecules.famille_id = familles.id) as NbMolecule FROM familles WHERE Nom = :famille";
        $sqlListEchantillon                     = "SELECT echantillon.Nom, echantillon.ReferenceLacapa, echantillon.id, echantillon.Urgent, C.Abrege as Client, echantillon.DateArriv as DateArriv FROM echantillon INNER JOIN molecules_echantillon ON molecules_echantillon.echantillon_id = echantillon.id INNER JOIN client C ON echantillon.client_id = C.id WHERE molecules_echantillon.InPaillasse = 0 AND echantillon.Supp = 0 group by echantillon.id ORDER BY echantillon.DateCreation DESC LIMIT ".$offset.", $perPage";
        $sqlFamilleForEchantillon               = "SELECT F.Nom FROM molecules_echantillon INNER JOIN molecules M ON molecules_echantillon.molecules_id = M.id INNER JOIN familles F ON M.famille_id = F.id INNER JOIN echantillon E ON molecules_echantillon.echantillon_id = E.id WHERE E.id = :echantillon AND InPaillasse = 0 GROUP BY F.id";
        $sqlNbMoleculeForEchantillonAndFamille  = "SELECT count(*) as nb FROM molecules_echantillon INNER JOIN molecules M ON molecules_echantillon.molecules_id = M.id INNER JOIN familles F ON M.famille_id = F.id WHERE F.Nom = :famille AND echantillon_id = :echantillon AND InPaillasse = 0";
        $sqlListMoleculeInEchantillonByFamille  = "SELECT M.Nom FROM molecules_echantillon INNER JOIN molecules M ON molecules_echantillon.molecules_id = M.id INNER JOIN familles F ON M.famille_id = F.id WHERE echantillon_id = :echantillon AND F.Nom = :famille AND InPaillasse = 0 ORDER BY M.Nom ASC";


        $listEchantillon    = $conn->query($sqlListEchantillon)->fetchAll(PDO::FETCH_ASSOC);
        $nbGlobal           = count($conn->query($sqlNbTotalEchantillon)->fetchAll(PDO::FETCH_ASSOC));

        $nbPage = ceil($nbGlobal / $perPage);

        foreach ($listEchantillon as $echantillon){

            $stmt = $conn->prepare($sqlFamilleForEchantillon);
            $stmt->bindValue("echantillon", $echantillon['id']);
            $stmt->execute();
            $familles = $stmt->fetchAll(PDO::FETCH_ASSOC);

            $row["echantillon"] = $echantillon;

            $row['analyse'] = [];
            foreach ($familles as $famille){

                $stmt2 = $conn->prepare($sqlFamilleCount);
                $stmt2->bindValue("famille", $famille['Nom']);
                $stmt2->execute();
                // $nbTotalMolecule = ($stmt2->fetch(PDO::FETCH_ASSOC))['NbMolecule'];

                $stmt3 = $conn->prepare($sqlNbMoleculeForEchantillonAndFamille);
                $stmt3->bindValue("famille", $famille['Nom']);
                $stmt3->bindValue("echantillon", $echantillon['id']);
                $stmt3->execute();
                $nbMoleculeInEchantillon = ($stmt3->fetch(PDO::FETCH_ASSOC))['nb'];

                if ($nbMoleculeInEchantillon >= 20){
                    $res = "Tous ".$famille['Nom'];
                }else {
                    $stmt4 = $conn->prepare($sqlListMoleculeInEchantillonByFamille);
                    $stmt4->bindValue("famille", $famille['Nom']);
                    $stmt4->bindValue("echantillon", $echantillon['id']);
                    $stmt4->execute();
                    $res = $stmt4->fetchAll(PDO::FETCH_COLUMN);
                }
                $row['analyse'][] = $res;
            }
            $listDisplay[] = $row;
        }

        return $this->render('UtilisateursBundle:Default:index.html.twig', [
            "user"          => $user,
            "listAnalyse"   => $listDisplay,
            "currentPage"   => $page,
            "nbPage"        => $nbPage
        ]);
    }

    public function listAction(){
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository("UtilisateursBundle:Utilisateurs")->findAll();
        return $this->render("UtilisateursBundle:Default:list.html.twig", array("utilisateurs" => $users));
    }

    public function ficheAction($id)
    {
            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository("UtilisateursBundle:Utilisateurs")->find($id);
            $logs = $em->getRepository("LogsBundle:Logs")->findBy(["utilisateurs" => $user]);

            return $this->render("@Utilisateurs/Default/fiche.html.twig", [
                "user" => $user,
                "logs" => $logs,
            ]);
    }

    public function modifFormAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository("UtilisateursBundle:Utilisateurs")->find($id);
        $form = $this->createForm(UpdateUserType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted()){
            $userManager = $this->get("fos_user.user_manager");
            $user->setDateModif(new \DateTime());
            $userManager->updateUser($form->getData());
            $this->addFlash("success", "L'utilisateur ".$user->getUsername()." a bien été modifié.");
            $this->get("app.log_manager")->creationLog("Modification de l'utilisateur ".$user->getNom());
        }

        return $this->render("@Utilisateurs/Default/modifForm.html.twig", [
            "form" => $form->createView(),
            "user" => $user
        ]);
    }

    public function modifFormPasswordAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository("UtilisateursBundle:Utilisateurs")->find($id);

        $formFactory = $this->container->get("fos_user.change_password.form.factory");
        $form = $formFactory->createForm();
        $form->remove("current_password");
        $form->add("submit", SubmitType::class, [
           "label" => "Mettre à jour",
            "attr" => [
                "class" => "btn-primary pull-right"
            ]
        ]);
        $form->setData($user);

        $form->handleRequest($request);
        if($form->isSubmitted()){
            $userManager = $this->get("fos_user.user_manager");
            $user->setDateModif(new \DateTime());
            $userManager->updateUser($form->getData());
            $this->addFlash("success", "Mot de passe modifié");
            $this->get("app.log_manager")->creationLog("Modification du mot de passe de ".$user->getNom());
        }

        return $this->render("@Utilisateurs/Default/changePassword.html.twig", [
            "form" => $form->createView(),
            "user" => $user
        ]);
    }

    public function disableUserAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository("UtilisateursBundle:Utilisateurs")->find($id);
        $userManager = $this->get("fos_user.user_manager");
        if($user->isEnabled()){
            $user->setEnabled(false);
            $this->addFlash("success", "Compte désactivé.");
            $this->get("app.log_manager")->creationLog("Désactivation du compte de ".$user->getNom());
        }else {
            $user->setEnabled(true);
            $this->addFlash("success", "Compte activé.");
            $this->get("app.log_manager")->creationLog("Activation du compte de ".$user->getNom());
        }
        $user->setDateModif(new \DateTime());
        $userManager->updateUser($user);
        return $this->redirectToRoute("utilisateurs_list");
    }

    public function addAction(Request $request)
    {
        $form = $this->createForm(RegistrationType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            $datas = $form->getData();
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $factory = $this->get('security.encoder_factory');

                $user = new Utilisateurs();
                $encoder = $factory->getEncoder($user);
                $user->setUsername($datas['username']);
                $user->setNom($datas['nom']);
                $user->setPrenom($datas['prenom']);

                $email = uniqid().'@lacapa.fr';
                $user->setEmail($email);
                $user->setEmailCanonical($email);

                $user->setPassword($encoder->encodePassword($datas['password'], $user->getSalt()));
                $user->setRoles([$datas['roles']]);
                $user->setPoste($datas['poste']);
                $user->setEnabled(true);

                $em->persist($user);
                $em->flush();

                $this->addFlash("success", "Utilisateur créé");
                $this->get("app.log_manager")->creationLog("Création du compte de ".$user->getNom());
            }
        }
        return $this->render('UtilisateursBundle:Default:create.html.twig', [
            'form'  => $form->createView()
        ]);
    }
}
