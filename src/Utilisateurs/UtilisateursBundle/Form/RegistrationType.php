<?php

namespace Utilisateurs\UtilisateursBundle\Form;

use FOS\UserBundle\Form\Type\UsernameFormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Utilisateurs\UtilisateursBundle\Entity\Utilisateurs;

class RegistrationType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            /*->add("email", EmailType::class,[
            "label" => "form.email",
            "translation_domain" => "FOSUserBundle",
            'required'  => false
        ])*/
            ->add('username', TextType::class, [
                'label' => "Nom d'utilisateur"
            ])
            ->add('nom', TextType::class)
            ->add("prenom", TextType::class, array("label" => "Prénom"))
            ->add('password', PasswordType::class, [
                'label' => 'Mot de passe'
            ])
            ->add("poste", TextType::class, [
                "label" => "Fonction"
            ])
            ->add("roles", ChoiceType::class, [
                "choices" => [
                    "Direction"             => "ROLE_ADMIN",
                    "Adminitratif"          => "ROLE_ADMINISTRATIF",
                    "Responsable technique" => "ROLE_RES_TECH",
                    "Technicien"            => "ROLE_TECH",
                    "Stagiaire / Non Permanent"             => "ROLE_USER"
                ]
            ]);
    }

    /*public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }*/

    public function configureOptions(OptionsResolver $resolver)
    {
        /*$resolver->setDefaults(array(
            'data_class' => Utilisateurs::class,
        ));*/
    }
}