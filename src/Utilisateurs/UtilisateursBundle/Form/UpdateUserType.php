<?php
/**
 * Created by PhpStorm.
 * User: deshiloh
 * Date: 03/07/2017
 * Time: 08:59
 */

namespace Utilisateurs\UtilisateursBundle\Form;


use function Sodium\add;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class UpdateUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("email", EmailType::class,[
                "label" => "form.email",
                "translation_domain" => "FOSUserBundle",
                'required'  => false
            ])
            ->add("username", TextType::class, [
                "label" => "form.username",
                "translation_domain" => "FOSUserBundle"
            ])
            ->add("nom", TextType::class)
            ->add("prenom", TextType::class)
            ->add("poste", TextType::class)
            ->add("roles", ChoiceType::class, [
                "choices" => [
                    "Direction"             => "ROLE_ADMIN",
                    "Adminitratif"          => "ROLE_ADMINISTRATIF",
                    "Responsable technique" => "ROLE_RES_TECH",
                    "Technicien"            => "ROLE_TECH",
                    "Stagiaire / Non Permanent"             => "ROLE_USER"
                ]
            ])
            ->add("submit", SubmitType::class, [
                "label" => "Modifier",
                "attr" => [
                    "class" => "btn-primary pull-right"
                ]
            ])
            ;

        $builder->get("roles")->addModelTransformer(new CallbackTransformer(
            function ($RolesAsArray){
                return $RolesAsArray[0];
            },
            function ($rolesAsString){
                return array($rolesAsString);
            }
        ));
    }
}