<?php

namespace Echantillon\EchantillonBundle\Controller;

use Doctrine\DBAL\DriverManager;
use Echantillon\EchantillonBundle\Entity\Echantillon;
use Echantillon\EchantillonBundle\Entity\MoleculesEchantillon;
use Echantillon\EchantillonBundle\Form\EchantillonType;
use Echantillon\EchantillonBundle\Form\MoleculeEchantillonType;
use Paillasse\PaillasseBundle\Entity\Paillasse;
use PDO;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class EchantillonController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $echantillonList = $em->getRepository("EchantillonBundle:Echantillon")->findAll();

        return $this->render('EchantillonBundle:Default:index.html.twig', [
            "listEchantillon" => $echantillonList,
        ]);
    }

    private function generateNewLacapaRef($referenceLacapa){

        $currentDate = new \DateTime();

        if($referenceLacapa === false){
            $year = substr(date("Y"), 2, 2);
            $referenceFinal = $year. "-1";
        }else {
            $r = explode("-", $referenceLacapa);
            $year = $r[0];
            $ref = intval($r[1]);

            $referenceFinal = $year . "-" .($ref + 1);
        }

        if ($currentDate->format("d") == "01" && $currentDate->format("m") == "01"){
            $referenceFinal = $year = substr(date("Y"), 2, 2). "-1";
        }

        return $referenceFinal;
    }

    public function AddAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $lastEchantillon = $em->getRepository("EchantillonBundle:Echantillon")->findLast();
        $action = "Ajout";
        if($lastEchantillon){
            $refLacapa = $this->generateNewLacapaRef($lastEchantillon->getReferenceLacapa());
        }else {
            $refLacapa = $this->generateNewLacapaRef(false);
        }

        $form = $this->createForm(EchantillonType::class);
        $form->get("ReferenceLacapa")->setData($refLacapa);
        $form->get("DateArriv")->setData(new \DateTime());
        $form->handleRequest($request);
        if($form->isSubmitted()){
            $echantillon = $form->getData();
            $echantillon->setImprime(false);
            $echantillon->setSupp(false);
            $echantillon->setDateCreation(new \DateTime());
            $echanTemp = $em->getRepository("EchantillonBundle:Echantillon")->findBy(["referenceLacapa" => $echantillon->getReferenceLacapa()]);
            foreach ($echanTemp as $item){
                if ($item->getReferenceLacapa() == $echantillon->getReferenceLacapa() && count($item->getMoleculesEchantillon()) == 0){
                    $em->remove($item);
                    $em->flush();
                }
            }
            $em->persist($echantillon);
            $em->flush();


            return $this->redirectToRoute("echantillon_add_molecules", ["id" => $echantillon->getId()]);
        }
        return $this->render("@Echantillon/Default/echantillon_form.html.twig", [
           "form"               => $form->createView(),
            "referenceLacapa"   => $refLacapa,
            "canNotModify"      => "false",
            "action"            => $action
        ]);
    }

    public function AddMoleculesFormAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $echantillon = $em->getRepository("EchantillonBundle:Echantillon")->find($id);

        $familles = $em->getRepository("EchantillonBundle:MoleculesEchantillon")->getFamillesResults($echantillon->getId());
        $test = [];
        foreach ($familles as $famille){
            $molecules  = $em->getRepository("EchantillonBundle:MoleculesEchantillon")->getMoleculesByFamille($famille["nom"], $echantillon->getId());
            $row["famille"] = $famille["nom"];
            $row["echantillons"] = $molecules;
            $test[] = $row;
        }


        $form = $this->createForm(MoleculeEchantillonType::class);

        $form->handleRequest($request);

        if($form->isSubmitted()){

            $this->addFlash("success", "Echantillon créé.");
            $this->get("app.log_manager")->creationLog("Création de l'échantillon ".$echantillon->getReferenceLacapa());
            return $this->redirectToRoute("echantillon_homepage");
        }

        return $this->render("EchantillonBundle:Default:echantillon_form_molecules.html.twig", [
            "echantillon"           => $echantillon,
            "form"                  => $form->createView(),
            "moleculesEchantillon"  => $test
        ]);
    }

    public function UpdateAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $action = "Modification";
        $echantillon = $em->getRepository("EchantillonBundle:Echantillon")->find($id);
        $nb = $em->getRepository("PaillasseBundle:PaillasseMolecule")->getCount($id);


        $form = $this->createForm(EchantillonType::class, $echantillon, [
            "submit_button_label" => "Modifier",
            "isMapped" => true,
        ]);
        /*$form->get("moleculesEchantillon")->setData($originalMolecules);*/

        /*if($nb > 0){
            $form->remove("submit");
        }*/

        /*$canNotModify = ($nb > 0);*/

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            /*$echantillonForm = $form->getData();*/
            /*foreach ($echantillonForm->getMoleculesEchantillon() as $moleculeEchantillon){
                $echantillonForm->removeMoleculesEchantillon($moleculeEchantillon);
                $em->remove($moleculeEchantillon);
            }
            foreach ($form->get("moleculesEchantillon")->getData() as $molecule){
                $moleculeEchantillon = new MoleculesEchantillon();
                $moleculeEchantillon->setIsAnalyse(false);
                $moleculeEchantillon->setEchantillon($echantillon);
                $moleculeEchantillon->setMolecules($molecule);
                $echantillonForm->addMoleculesEchantillon($moleculeEchantillon);
            }*/
            $em->flush();
            $this->addFlash("success", "Votre modification a bien été prise en compte.");
            $this->get("app.log_manager")->creationLog("Modification de l'échantillon ".$echantillon->getReferenceLacapa());

            return $this->redirectToRoute("echantillon_update", ["id" => $echantillon->getId()]);
        }

        return $this->render("@Echantillon/Default/echantillon_form.html.twig", [
           "form"               => $form->createView(),
            "referenceLacapa"   => $echantillon->getReferenceLacapa(),
            "action"            => $action
        ]);
    }

    /**
     * @param $id
     * @param  Request  $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function UpdateMoleculeAction($id, Request $request)
    {
        $em =$this->getDoctrine()->getManager();
        $conn = $this->getDoctrine()->getConnection();
        $echantillon = $em->getRepository("EchantillonBundle:Echantillon")->find($id);
        $test = [];
        if ($request->isMethod("POST")){
            $datas = $request->request;
            if ($datas->get("familles")){
                foreach ($datas->get("familles") as $famille){

                    $sql = "SELECT molecules_echantillon.* FROM molecules_echantillon 
JOIN Molecules M on molecules_echantillon.molecules_id = M.id JOIN Familles F on M.famille_id = F.id 
JOIN Echantillon E on molecules_echantillon.echantillon_id = E.id 
WHERE F.Nom LIKE :famille AND E.id = :echantillon";

                    /** @var \PDOStatement $stmt */
                    $stmt = $conn->prepare($sql);
                    $stmt->bindValue('famille', "%$famille%");
                    $stmt->bindValue('echantillon', $id);
                    $stmt->execute();
                    $moleculesEchantillon = $stmt->fetchAll();


                    //$molecules = $em->getRepository("EchantillonBundle:MoleculesEchantillon")->getMoleculesByFamille($famille, $id);
                    foreach ($moleculesEchantillon as $moleculeechantillon){
                        
                        $stmt = $conn->prepare("DELETE FROM paillasse_molecule WHERE molecule_echantillon_id = :idME");
                        $stmt->bindValue('idME', $moleculeechantillon['id']);
                        $stmt->execute();

                        $stmt = $conn->prepare("DELETE FROM molecules_echantillon WHERE id = :idME");
                        $stmt->bindValue('idME', $moleculeechantillon['id']);
                        $stmt->execute();
                    }
                }
            }

            if ($datas->get("molecules")){
                foreach ($datas->get("molecules") as $idMolecule){
                    $molecule = $em->getRepository("EchantillonBundle:MoleculesEchantillon")->findOneBy([
                        "id" => $idMolecule
                    ]);
                    $moleculePaillase = $em->getRepository("PaillasseBundle:PaillasseMolecule")->findOneBy([
                        "moleculeEchantillon" => $molecule
                    ]);
                    if ($moleculePaillase){
                        $em->remove($moleculePaillase);
                        $em->flush();
                    }
                    $echantillon->removeMoleculesEchantillon($molecule);
                    $em->remove($molecule);
                    $em->flush();
                }
            }
        }

        $familles = $em->getRepository("EchantillonBundle:MoleculesEchantillon")->getFamillesResults($echantillon->getId());

        foreach ($familles as $famille){
            $molecules  = $em->getRepository("EchantillonBundle:MoleculesEchantillon")->getMoleculesByFamille($famille["nom"], $echantillon->getId());
            $row["famille"] = $famille["nom"];
            $row["echantillons"] = $molecules;
            $test[] = $row;
        }


       /* $originalMolecules = new ArrayCollection();
        foreach ($echantillon->getMoleculesEchantillon() as $molecule){
            $originalMolecules->add($molecule->getMolecules());
        }*/

        $form = $this->createForm(MoleculeEchantillonType::class);
        /*$form->get("moleculesEchantillon")->setData($originalMolecules);*/

        $form->handleRequest($request);
        if($form->isSubmitted()){
            /*foreach ($echantillon->getMoleculesEchantillon() as $moleculeEchantillon){
                $echantillon->removeMoleculesEchantillon($moleculeEchantillon);
                $em->remove($moleculeEchantillon);
            }
            foreach ($form->get("moleculesEchantillon")->getData() as $molecule){
                $moleculeEchantillon = new MoleculesEchantillon();
                $moleculeEchantillon->setIsAnalyse(false);
                $moleculeEchantillon->setEchantillon($echantillon);
                $moleculeEchantillon->setMolecules($molecule);
                $echantillon->addMoleculesEchantillon($moleculeEchantillon);
            }
            $em->flush();*/
            $this->addFlash("success", "Votre modification a bien été prise en compte.");
            $this->get("app.log_manager")->creationLog("Modification des molécules de l'échantillon ".$echantillon->getReferenceLacapa());
            return $this->redirectToRoute("echantillon_homepage");
        }

        return $this->render("@Echantillon/Default/echantillon_form_molecules.html.twig", [
            "echantillon"           => $echantillon,
            "form"                  => $form->createView(),
            "moleculesEchantillon"   => $test
        ]);
    }

    public function addFamilleAction(Request $request, $id)
    {
        $em             = $this->getDoctrine()->getManager();
        $conn           = $this->getDoctrine()->getConnection();
        $echantillon    = $em->getRepository("EchantillonBundle:Echantillon")->find($id);

        $sql = "SELECT Molecules.* FROM Molecules JOIN Familles F on Molecules.famille_id = F.id WHERE F.id = :famille AND Molecules.actif = 1";
        /** @var \PDOStatement $stmt */
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('famille', $request->query->get('idFamille'));
        $stmt->execute();
        $molecules = $stmt->fetchAll();

        /*$molecules      = $em->getRepository("MoleculesBundle:Molecules")->findBy([
            "famille"   => $_POST["idFamille"],
            'actif'     => true
        ]);*/

        $sql = "SELECT * FROM molecules_echantillon WHERE echantillon_id = :echantillon";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('echantillon', $echantillon->getId());
        $stmt->execute();
        $moleculesInEchantillon = $stmt->fetchAll();

        /*$moleculesInEchantillon = $em->getRepository("EchantillonBundle:MoleculesEchantillon")->findBy([
            "echantillon" => $echantillon
        ]);*/

        $datas = [];
        foreach ($moleculesInEchantillon as $item) {
            $datas[] = $item['molecules_id'];
        }

        foreach ($molecules as $molecule){

            if (!in_array($molecule['id'], $datas)){

                $sql    = "INSERT INTO molecules_echantillon (molecules_id, echantillon_id, isAnalyse, inPaillasse) VALUES (:molecule, :echantillon, :analyse, :paillasse)";
                $stmt   = $conn->prepare($sql);
                $stmt->bindValue('molecule', $molecule['id']);
                $stmt->bindValue('echantillon', $echantillon->getId());
                $stmt->bindValue('analyse', 0);
                $stmt->bindValue('paillasse', 0);
                $stmt->execute();

                /*$moltest = new MoleculesEchantillon();
                $moltest->setMolecules($molecule);
                $moltest->setEchantillon($echantillon);
                $moltest->setIsAnalyse(false);
                $moltest->setInPaillasse(false);
                $em->persist($moltest);
                $em->flush();*/
            }
        }
        return new JsonResponse();
    }

    public function addModeleAction($id)
    {
        $em             = $this->getDoctrine()->getManager();
        $echantillon    = $em->getRepository("EchantillonBundle:Echantillon")->find($id);
        $modele         = $em->getRepository("AnalyseBundle:ModeleAnalyse")->find($_POST["idModele"]);

        foreach ($modele->getMolecules() as $idMolecule){
            $molecule               = $em->getRepository("MoleculesBundle:Molecules")->find($idMolecule);
            $moleculeInEchantillon  = $em->getRepository("EchantillonBundle:MoleculesEchantillon")->findOneBy([
                "echantillon"   => $echantillon,
                "molecules"     => $molecule
            ]);

            if (!$moleculeInEchantillon && $molecule->getActif()){
                $moleculeEchantillon = new MoleculesEchantillon();
                $moleculeEchantillon->setMolecules($molecule);
                $moleculeEchantillon->setEchantillon($echantillon);
                $moleculeEchantillon->setIsAnalyse(false);
                $em->persist($moleculeEchantillon);
                $em->flush();
            }
        }
        return new JsonResponse([]);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function addMoleculeAction($id, $idMolecule)
    {
        $em             = $this->getDoctrine()->getManager();
        $echantillon    = $em->getRepository("EchantillonBundle:Echantillon")->find($id);
        $molecule       = $em->getRepository("MoleculesBundle:Molecules")->find($idMolecule);

        $moleculeInEchantillon = $em->getRepository("EchantillonBundle:MoleculesEchantillon")->findOneBy([
            "echantillon"   => $echantillon,
            "molecules"     => $molecule
        ]);

        if (!$moleculeInEchantillon){
            $moleculeEchantillon = new MoleculesEchantillon();
            $moleculeEchantillon->setMolecules($molecule);
            $moleculeEchantillon->setEchantillon($echantillon);
            $moleculeEchantillon->setIsAnalyse(false);
            dump($moleculeEchantillon);
            $em->persist($moleculeEchantillon);
            $em->flush();
        }

        return new JsonResponse([]);
    }

    public function cancelPrintListAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        if ($request->isMethod("POST")){
            $echantillonsToPrint = $request->request->get("echantillon");
            foreach ($echantillonsToPrint as $echantillonID){
                $echantillon = $em->getRepository("EchantillonBundle:Echantillon")->find($echantillonID);
                $echantillon->setImprime(false);
                $em->flush();
            }

            $this->addFlash("success", "Opéraion réussite.");
        }

        $listEchantillon = $em->getRepository("EchantillonBundle:Echantillon")->findBy([
            "imprime" => true
        ], ["id" => "desc"]);

        return $this->render("@Echantillon/Default/echantillon_print_list.html.twig", [
            "listEchantillon"   => $listEchantillon
        ]);
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $echantillon = $em->getRepository("EchantillonBundle:Echantillon")->find($id);

        $echantillon->setSupp(true);
        $em->flush();

        $this->addFlash("success", "Echantillon ".$echantillon->getReferenceLacapa()." annulé.");
        $this->get("app.log_manager")->creationLog("Annulation de l'échantillon ".$echantillon->getReferenceLacapa());
        return $this->redirectToRoute("echantillon_homepage");
    }

    public function listJSONAction()
    {
        $em = $this->getDoctrine()->getManager();
        $moleculesEchantillons = $em->getRepository("EchantillonBundle:MoleculesEchantillon")->findAll();
        $res = array();
        foreach ($moleculesEchantillons as $moleculesEchantillon){
            $molecule       = $em->getRepository("MoleculesBundle:Molecules")->getArrayById($moleculesEchantillon->getMolecules()->getId());
            $echantillon    = $em->getRepository("EchantillonBundle:Echantillon")->find($moleculesEchantillon->getEchantillon()->getId());
            $datas["id"] = $moleculesEchantillon->getId();
            $datas["molecule"] = $molecule;
            $datas["echantillon"] = $echantillon->getReferenceLacapa();
            $res[] = $datas;
        }
        return new JsonResponse($res);
    }

    public function moleculeJSONAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $moleculeEchantillon = $em->getRepository("EchantillonBundle:MoleculesEchantillon")->getArrayMoleculeByID($id);
        return new JsonResponse($moleculeEchantillon);
    }

    public function printAction(Request $request)
    {
        require $this->get("kernel")->getProjectDir() . "/web/fpdf/PDF_Label.php";

        $em = $this->getDoctrine()->getManager();
        $collection = [];

        if ($request->isMethod("POST")) {
            $echantillonsToPrint = $request->request->get("echantillon");
            foreach ($echantillonsToPrint as $idEchantillon){

                $echantillon    = $em->getRepository("EchantillonBundle:Echantillon")->find($idEchantillon);
                $familles       = $em->getRepository("EchantillonBundle:Echantillon")->getFamilles($idEchantillon);
                $famillesDatas = [];


                foreach ($familles as $famille){
                    $famillesDatas[] = $famille['nom'];
                }
                if ($echantillon){
                    $echantillon->setImprime(true);
                    $em->flush();

                    $datas['familles']      = $famillesDatas;
                    $datas['echantillon']   = $echantillon;

                    $collection[]   = $datas;
                }
            }
            $pdf = new \PDF_Label("L7163");
            $pdf->AddPage();

            foreach ($collection as $row){
                $urgent = ($row["echantillon"]->getUrgent()) ? 'Urgent' : '';
                $ref    = $row["echantillon"]->getReferenceLacapa() . " " . utf8_decode("Reçu le : ".$row["echantillon"]->getDateArriv()->format("d/m/Y")) ." " . $urgent;
                $text   = sprintf("%s\n%s\n%s\n%s\n%s", $ref, utf8_decode($row["echantillon"]->getClient()->getAbrege()), utf8_decode($row["echantillon"]->getNom()), utf8_decode($row["echantillon"]->getReferenceClient()), utf8_decode(implode(", ", $row["familles"])));

                $pdf->Add_Label($text);
            }

            $pdf->Output();
            die();
            /*return $this->render("@Echantillon/Default/echantillon_print_echantillons.html.twig", [
                "listEchantillons"  => $collection
            ]);*/
        }else {
            $this->redirectToRoute("echantillon_print_list");
        }

    }

    public function printListAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $listEchantillon = $em->getRepository("EchantillonBundle:Echantillon")->findBy([
            "imprime" => false
        ], ["id" => "desc"]);

        return $this->render("@Echantillon/Default/echantillon_print_list.html.twig", [
            "listEchantillon"   => $listEchantillon
        ]);
    }

    public function unDeleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $echantillon = $em->getRepository("EchantillonBundle:Echantillon")->find($id);

        $echantillon->setSupp(false);
        $em->flush();

        $this->addFlash("success", "Echantillon ".$echantillon->getReferenceLacapa()." n'est plus annulé.");
        $this->get("app.log_manager")->creationLog("Annulation de l'échantillon ".$echantillon->getReferenceLacapa());
        return $this->redirectToRoute("echantillon_homepage");
    }

    public function familleListJSONAction()
    {
        $em = $this->getDoctrine()->getManager();
        $familles = $em->getRepository("EchantillonBundle:MoleculesEchantillon")->getFamillesArray();
        return new JsonResponse($familles);
    }

    public function ficheAction($id, $mode)
    {
        $em = $this->getDoctrine()->getManager();
        $molecuesEchantillon = [];
        $echantillon = $em->getRepository("EchantillonBundle:Echantillon")->find($id);

        $familles = $em->getRepository("EchantillonBundle:MoleculesEchantillon")->getFamillesResultsFiche($echantillon->getId(), $mode);

        foreach ($familles as $famille){
            $paillasse = $this->getPaillasseByFamilleForEchantillon($echantillon, $famille['nom']);
            $molecules  = $em->getRepository("EchantillonBundle:MoleculesEchantillon")->getMoleculesByFamilleFiche($famille["nom"], $echantillon->getId(), $mode);

            $row["famille"]         = $famille["nom"];
            $row["echantillons"]    = $molecules;
            $row['paillasse']       = $paillasse;

            $molecuesEchantillon[] = $row;
        }
        /*$moleculesEchantillon = $em->getRepository("EchantillonBundle:MoleculesEchantillon")->findBy(["echantillon" => $id]);*/

        return $this->render("EchantillonBundle:Default:echantillon_fiche.html.twig", [
            "moleculesEchantillon"  => $molecuesEchantillon,
            "echantillon"           => $echantillon,
            "mode"                  => $mode
        ]);
    }

    public function familleJSONAction($nom)
    {
        $em = $this->getDoctrine()->getManager();
        $molecules = $em->getRepository("EchantillonBundle:MoleculesEchantillon")->getMoleculesByFamillArray($nom);
        return new JsonResponse($molecules);
    }

    /**
     * Récupère la dernière fiche de paillasse active
     * @param  Echantillon  $echantillon
     * @param  String  $famille
     * @return mixed
     */
    private function getPaillasseByFamilleForEchantillon($echantillon, $famille) {
        /** @var PDO $conn */
        $conn = $this->getDoctrine()->getConnection();
        $sql = "SELECT paillasse.* FROM paillasse
JOIN paillasse_molecule pm on paillasse.id = pm.paillasse_id
JOIN molecules_echantillon me on pm.molecule_echantillon_id = me.id
JOIN echantillon E on me.echantillon_id = E.id
JOIN molecules M on me.molecules_id = M.id
JOIN familles F on M.famille_id = F.id
WHERE F.Nom LIKE :famille AND paillasse.actif = 1 AND E.id = :echantillon
ORDER BY paillasse.dateCreation DESC
";
        $stmt = $conn->prepare($sql);
        $stmt->execute([
           'famille'        => '%'.$famille.'%',
           'echantillon'    => $echantillon->getId()
        ]);
        return $stmt->fetch();
    }
}
