<?php
/**
 * Created by PhpStorm.
 * User: deshiloh
 * Date: 11/08/2017
 * Time: 19:02
 */

namespace Echantillon\EchantillonBundle\Form\DataTransformer;


use Doctrine\Common\Persistence\ObjectManager;
use Molecules\MoleculesBundle\Entity\Molecules;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class MoleculeToStringTransformer implements DataTransformerInterface
{
    private $em;

    public function __construct(ObjectManager $manager)
    {
        $this->em = $manager;
    }

    /**
     * Transform molecule objet to string
     * @param $molecules Molecules[]
     */
    public function transform($molecules)
    {
        if($molecules){
            $datas = array();
            foreach ($molecules as $molecule){
                $datas[] = $molecule->getID();
            }
            $moleculesAsString = implode("|", $datas);
            return $moleculesAsString;
        }else {
            return "";
        }
    }

    /**
     * Transforme String Array Molecule en Object Molecule
     * @param mixed $stringArrayMolecule
     */
    public function reverseTransform($stringArrayMolecule)
    {

        $datas = array();
        $r = explode("|", $stringArrayMolecule);
        foreach ($r as $id){
            $molecule = $this->em->getRepository("MoleculesBundle:Molecules")->find($id);
            if(!$molecule){
                throw new TransformationFailedException(sprintf("Une erreur", $stringArrayMolecule));
            }else {
                $datas[] = $molecule;
            }
        }
        return $datas;
    }
}