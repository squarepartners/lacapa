<?php

namespace Echantillon\EchantillonBundle\Form;


use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityRepository;
use Echantillon\EchantillonBundle\Entity\Echantillon;
use Echantillon\EchantillonBundle\Form\DataTransformer\MoleculeToStringTransformer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EchantillonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("client", EntityType::class, [
                "class" => "Client\ClientBundle\Entity\Client",
                "query_builder" => function (EntityRepository $er){
                    return $er->createQueryBuilder("c")->orderBy("c.abrege", "ASC");
                },
                "choice_label" => "Abrege"
            ])
            ->add("Nom", TextType::class)
            ->add("ReferenceLacapa", HiddenType::class)
            ->add("ReferenceClient", TextType::class, [
                "label"     => "Référence Client",
                "required"  => false
            ])
            ->add("Poids", TextType::class, [
                "label" => "Poids",
            ])
            ->add("Urgent", CheckboxType::class, [
                "required" => false,
                "attr" => [
                    "class" => "checkBoxTheme"
                ]
            ])
            //TODO[deshiloh] Faire un place holder pour inviter le choix de la date
            ->add("DateArriv", DateTimeType::class, [
                "label" => "Date d'arrivée",
                /*"data" => new \DateTime("now")*/
            ])
            //TODO[deshiloh] Faire le paramètre de la date butoir à mettre dans le controller
            ->add("DateBut", DateTimeType::class, [
                "label" => "Date butoir",
                "data" => new \DateTime("now +7 day")
            ])
            /*->add("moleculesEchantillon", HiddenType::class, [
                "mapped" => false,
                "invalid_message" => "Il faut au moins une molécule",
                "attr" => [
                    "class" => "molecules_datas"
                ]
            ])*/
            ->add("Commentaire", TextareaType::class, [
                "required" => false,
            ])
            ->add("submit", SubmitType::class, [
                "attr" => [
                    "class" => "btn-primary btn-sm pull-right"
                ],
                "label" => $options["submit_button_label"]
            ])
        ;

        /*$builder->get("moleculesEchantillon")->addModelTransformer(new MoleculeToStringTransformer($this->em));*/
        $builder->get("Urgent")->addModelTransformer(new CallbackTransformer(
           function ($test){
               return ($test == 0) ? false : true;
           }, function($test2){
            return ($test2 == false) ? 0 : 1;
            }
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
           "data_class" => Echantillon::class,
            "submit_button_label" => "Étape suivante",
            "isMapped" => false
        ]);
    }
}