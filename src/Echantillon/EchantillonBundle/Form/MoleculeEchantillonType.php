<?php

namespace Echantillon\EchantillonBundle\Form;


use Doctrine\Common\Persistence\ObjectManager;
use Echantillon\EchantillonBundle\Form\DataTransformer\MoleculeToStringTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

class MoleculeEchantillonType extends AbstractType
{
    private $em;

    public function __construct(ObjectManager $manager)
    {
        $this->em = $manager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("moleculesEchantillon", HiddenType::class, [
                "mapped" => false,
                "invalid_message" => "Il faut au moins une molécule",
                "attr" => [
                    "class" => "molecules_datas"
                ]
            ])
            ->add("Submit", SubmitType::class, [
                "label" => "Valider l'échantillon",
                "attr"  => [
                    "class" => "btn-primary"
                ]
            ])
        ;

        $builder->get("moleculesEchantillon")->addModelTransformer(new MoleculeToStringTransformer($this->em));
    }
}