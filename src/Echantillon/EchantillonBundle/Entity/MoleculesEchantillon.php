<?php

namespace Echantillon\EchantillonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Molecules\MoleculesBundle\Entity\Molecules;
use Paillasse\PaillasseBundle\Entity\PaillasseMolecule;

/**
 * MoleculesEchantillon
 *
 * @ORM\Table(name="molecules_echantillon")
 * @ORM\Entity(repositoryClass="Echantillon\EchantillonBundle\Repository\MoleculesEchantillonRepository")
 */
class MoleculesEchantillon
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="isAnalyse", type="boolean")
     */
    private $isAnalyse;

    /**
     * @var string
     * @ORM\Column(name="Resultat", type="string", length=100, nullable=true)
     */
    private $resulat;

    /**
     * @var int
     * @ORM\Column(name="tauxRecup", type="integer", nullable=true, length=3)
     */
    private $tauxRecup;

    /**
     * @var string
     * @ORM\Column(name="Commentaire", type="blob", nullable=true)
     */
    private $commentaire;

    /**
     * @var \DateTime
     * @ORM\Column(name="DateAnalyse", type="datetime", nullable=true)
     */
    private $dateAnalyse;

    /**
     * @var boolean
     * @ORM\Column(name="InPaillasse", type="boolean", nullable=true)
     */
    private $inPaillasse = 0;

    /**
     * @var Molecules
     * @ORM\ManyToOne(targetEntity="Molecules\MoleculesBundle\Entity\Molecules", inversedBy="moleculesEchantillon")
     */
    private $molecules;

    /**
     * @var Echantillon
     * @ORM\ManyToOne(targetEntity="Echantillon\EchantillonBundle\Entity\Echantillon", inversedBy="moleculesEchantillon")
     */
    private $echantillon;

    /**
     * @var PaillasseMolecule
     * @ORM\OneToMany(targetEntity="Paillasse\PaillasseBundle\Entity\PaillasseMolecule", mappedBy="moleculeEchantillon")
     */
    private $moleculePaillass;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isAnalyse
     *
     * @param boolean $isAnalyse
     *
     * @return MoleculesEchantillon
     */
    public function setIsAnalyse($isAnalyse)
    {
        $this->isAnalyse = $isAnalyse;

        return $this;
    }

    /**
     * Get isAnalyse
     *
     * @return bool
     */
    public function getIsAnalyse()
    {
        return $this->isAnalyse;
    }

    /**
     * Set echantillon
     *
     * @param \Echantillon\EchantillonBundle\Entity\Echantillon $echantillon
     *
     * @return MoleculesEchantillon
     */
    public function setEchantillon(\Echantillon\EchantillonBundle\Entity\Echantillon $echantillon = null)
    {
        $this->echantillon = $echantillon;

        return $this;
    }

    /**
     * Get echantillon
     *
     * @return \Echantillon\EchantillonBundle\Entity\Echantillon
     */
    public function getEchantillon()
    {
        return $this->echantillon;
    }

    /**
     * Set molecules
     *
     * @param \Molecules\MoleculesBundle\Entity\Molecules $molecules
     *
     * @return MoleculesEchantillon
     */
    public function setMolecules(\Molecules\MoleculesBundle\Entity\Molecules $molecules = null)
    {
        $this->molecules = $molecules;

        return $this;
    }

    /**
     * Get molecules
     *
     * @return \Molecules\MoleculesBundle\Entity\Molecules
     */
    public function getMolecules()
    {
        return $this->molecules;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->paillasse = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add paillasse
     *
     * @param \Paillasse\PaillasseBundle\Entity\Paillasse $paillasse
     *
     * @return MoleculesEchantillon
     */
    public function addPaillasse(\Paillasse\PaillasseBundle\Entity\Paillasse $paillasse)
    {
        $this->paillasse[] = $paillasse;

        return $this;
    }

    /**
     * Remove paillasse
     *
     * @param \Paillasse\PaillasseBundle\Entity\Paillasse $paillasse
     */
    public function removePaillasse(\Paillasse\PaillasseBundle\Entity\Paillasse $paillasse)
    {
        $this->paillasse->removeElement($paillasse);
    }

    /**
     * Get paillasse
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPaillasse()
    {
        return $this->paillasse;
    }

    /**
     * Set resulat
     *
     * @param string $resulat
     *
     * @return MoleculesEchantillon
     */
    public function setResulat($resulat)
    {
        $this->resulat = $resulat;

        return $this;
    }

    /**
     * Get resulat
     *
     * @return string
     */
    public function getResulat()
    {
        return $this->resulat;
    }

    /**
     * Set commentaire
     *
     * @param string $commentaire
     *
     * @return MoleculesEchantillon
     */
    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    /**
     * Get commentaire
     *
     * @return string
     */
    public function getCommentaire()
    {
        if(!empty($this->commentaire)){
            return stream_get_contents($this->commentaire);
        }else {
            return "";
        }
    }

    /**
     * Set dateAnalyse
     *
     * @param \DateTime $dateAnalyse
     *
     * @return MoleculesEchantillon
     */
    public function setDateAnalyse($dateAnalyse)
    {
        $this->dateAnalyse = $dateAnalyse;

        return $this;
    }

    /**
     * Get dateAnalyse
     *
     * @return \DateTime
     */
    public function getDateAnalyse()
    {
        return $this->dateAnalyse;
    }

    /**
     * Set tauxRecup
     *
     * @param integer $tauxRecup
     *
     * @return MoleculesEchantillon
     */
    public function setTauxRecup($tauxRecup)
    {
        $this->tauxRecup = $tauxRecup;

        return $this;
    }

    /**
     * Get tauxRecup
     *
     * @return integer
     */
    public function getTauxRecup()
    {
        return $this->tauxRecup;
    }

    /**
     * Set inPaillasse
     *
     * @param boolean $inPaillasse
     *
     * @return MoleculesEchantillon
     */
    public function setInPaillasse($inPaillasse)
    {
        $this->inPaillasse = $inPaillasse;

        return $this;
    }

    /**
     * Get inPaillasse
     *
     * @return boolean
     */
    public function getInPaillasse()
    {
        return $this->inPaillasse;
    }

    /**
     * Add moleculePaillass
     *
     * @param \Paillasse\PaillasseBundle\Entity\PaillasseMolecule $moleculePaillass
     *
     * @return MoleculesEchantillon
     */
    public function addMoleculePaillass(\Paillasse\PaillasseBundle\Entity\PaillasseMolecule $moleculePaillass)
    {
        $this->moleculePaillass[] = $moleculePaillass;

        return $this;
    }

    /**
     * Remove moleculePaillass
     *
     * @param \Paillasse\PaillasseBundle\Entity\PaillasseMolecule $moleculePaillass
     */
    public function removeMoleculePaillass(\Paillasse\PaillasseBundle\Entity\PaillasseMolecule $moleculePaillass)
    {
        $this->moleculePaillass->removeElement($moleculePaillass);
    }

    /**
     * Get moleculePaillass
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMoleculePaillass()
    {
        return $this->moleculePaillass;
    }
}
