<?php

namespace Echantillon\EchantillonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Echantillon
 *
 * @ORM\Table(name="echantillon")
 * @ORM\Entity(repositoryClass="Echantillon\EchantillonBundle\Repository\EchantillonRepository")
 */
class Echantillon
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom", type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="ReferenceLacapa", type="string", length=255, unique=true)
     */
    private $referenceLacapa;

    /**
     * @var string
     *
     * @ORM\Column(name="ReferenceClient", type="string", length=255, nullable=true)
     */
    private $referenceClient;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateArriv", type="datetime")
     */
    private $dateArriv;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateBut", type="datetime")
     */
    private $dateBut;

    /**
     * @var string
     *
     * @ORM\Column(name="Commentaire", type="blob", nullable=true)
     */
    private $commentaire;

    /**
     * @var bool
     *
     * @ORM\Column(name="Urgent", type="boolean")
     */
    private $urgent;

    /**
     * @var string
     *
     * @ORM\Column(name="Poids", type="string", length=255, nullable=true)
     */
    private $poids;

    /**
     * @var bool
     *
     * @ORM\Column(name="Imprime", type="boolean")
     */
    private $imprime;

    /**
     * @var bool
     *
     * @ORM\Column(name="Supp", type="boolean")
     */
    private $supp;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateSupp", type="datetime", nullable=true)
     */
    private $dateSupp;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateCreation", type="datetime")
     */
    private $dateCreation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateModif", type="datetime", nullable=true)
     */
    private $dateModif;

    /**
     * @ORM\ManyToOne(targetEntity="Client\ClientBundle\Entity\Client", inversedBy="echantillons")
     */
    private $client;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     * @ORM\OneToMany(targetEntity="Echantillon\EchantillonBundle\Entity\MoleculesEchantillon", mappedBy="echantillon", cascade={"persist", "remove"})
     */
    private $moleculesEchantillon;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Echantillon
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set dateArriv
     *
     * @param \DateTime $dateArriv
     *
     * @return Echantillon
     */
    public function setDateArriv($dateArriv)
    {
        $this->dateArriv = $dateArriv;

        return $this;
    }

    /**
     * Get dateArriv
     *
     * @return \DateTime
     */
    public function getDateArriv()
    {
        return $this->dateArriv;
    }

    /**
     * Set dateBut
     *
     * @param \DateTime $dateBut
     *
     * @return Echantillon
     */
    public function setDateBut($dateBut)
    {
        $this->dateBut = $dateBut;

        return $this;
    }

    /**
     * Get dateBut
     *
     * @return \DateTime
     */
    public function getDateBut()
    {
        return $this->dateBut;
    }

    /**
     * Set urgent
     *
     * @param boolean $urgent
     *
     * @return Echantillon
     */
    public function setUrgent($urgent)
    {
        $this->urgent = $urgent;

        return $this;
    }

    /**
     * Get urgent
     *
     * @return bool
     */
    public function getUrgent()
    {
        return $this->urgent;
    }

    /**
     * Set poids
     *
     * @param string $poids
     *
     * @return Echantillon
     */
    public function setPoids($poids)
    {
        $this->poids = $poids;

        return $this;
    }

    /**
     * Get poids
     *
     * @return string
     */
    public function getPoids()
    {
        return $this->poids;
    }

    /**
     * Set imprime
     *
     * @param boolean $imprime
     *
     * @return Echantillon
     */
    public function setImprime($imprime)
    {
        $this->imprime = $imprime;

        return $this;
    }

    /**
     * Get imprime
     *
     * @return bool
     */
    public function getImprime()
    {
        return $this->imprime;
    }

    /**
     * Set supp
     *
     * @param boolean $supp
     *
     * @return Echantillon
     */
    public function setSupp($supp)
    {
        $this->supp = $supp;

        return $this;
    }

    /**
     * Get supp
     *
     * @return bool
     */
    public function getSupp()
    {
        return $this->supp;
    }

    /**
     * Set dateSupp
     *
     * @param \DateTime $dateSupp
     *
     * @return Echantillon
     */
    public function setDateSupp($dateSupp)
    {
        $this->dateSupp = $dateSupp;

        return $this;
    }

    /**
     * Get dateSupp
     *
     * @return \DateTime
     */
    public function getDateSupp()
    {
        return $this->dateSupp;
    }

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     *
     * @return Echantillon
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set dateModif
     *
     * @param \DateTime $dateModif
     *
     * @return Echantillon
     */
    public function setDateModif($dateModif)
    {
        $this->dateModif = $dateModif;

        return $this;
    }

    /**
     * Get dateModif
     *
     * @return \DateTime
     */
    public function getDateModif()
    {
        return $this->dateModif;
    }

    /**
     * Set client
     *
     * @param \Client\ClientBundle\Entity\Client $client
     *
     * @return Echantillon
     */
    public function setClient(\Client\ClientBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Client\ClientBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }


    /**
     * Set referenceClient
     *
     * @param string $referenceClient
     *
     * @return Echantillon
     */
    public function setReferenceClient($referenceClient)
    {
        $this->referenceClient = $referenceClient;

        return $this;
    }

    /**
     * Get referenceClient
     *
     * @return string
     */
    public function getReferenceClient()
    {
        return $this->referenceClient;
    }

    /**
     * Set commentaire
     *
     * @param string $commentaire
     *
     * @return Echantillon
     */
    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    /**
     * Get commentaire
     *
     * @return string
     */
    public function getCommentaire()
    {
        if(!empty($this->commentaire)){
            return stream_get_contents($this->commentaire);
        }else {
            return "";
        }
    }

    /**
     * Set referenceLacapa
     *
     * @param string $referenceLacapa
     *
     * @return Echantillon
     */
    public function setReferenceLacapa($referenceLacapa)
    {
        $this->referenceLacapa = $referenceLacapa;

        return $this;
    }

    /**
     * Get referenceLacapa
     *
     * @return string
     */
    public function getReferenceLacapa()
    {
        return $this->referenceLacapa;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->moleculesEchantillon = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add moleculesEchantillon
     *
     * @param \Echantillon\EchantillonBundle\Entity\MoleculesEchantillon $moleculesEchantillon
     *
     * @return Echantillon
     */
    public function addMoleculesEchantillon(\Echantillon\EchantillonBundle\Entity\MoleculesEchantillon $moleculesEchantillon)
    {
        $this->moleculesEchantillon[] = $moleculesEchantillon;

        return $this;
    }

    /**
     * Remove moleculesEchantillon
     *
     * @param \Echantillon\EchantillonBundle\Entity\MoleculesEchantillon $moleculesEchantillon
     */
    public function removeMoleculesEchantillon(\Echantillon\EchantillonBundle\Entity\MoleculesEchantillon $moleculesEchantillon)
    {
        $this->moleculesEchantillon->removeElement($moleculesEchantillon);
    }

    /**
     * Get moleculesEchantillon
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getMoleculesEchantillon()
    {
        return $this->moleculesEchantillon;
    }

    /**
     * Get nbNotAnalyse
     * @return int
     */
    public function nbNotAnalyse(){
        $count = 0;
        foreach ($this->moleculesEchantillon as $row){
            if($row->getIsAnalyse() === false){
                $count ++;
            }
        }
        return $count;
    }

    /**
     * Get nbAnalyse
     * @return int
     */
    public function nbAnalyse(){
        $count = 0;
        foreach ($this->moleculesEchantillon as $row){
            if($row->getIsAnalyse() === true){
                $count ++;
            }
        }
        return $count;
    }

    /**
     * @return mixed
     */
    public function getDateAnalyse(){
        $datas = [];
        foreach ($this->moleculesEchantillon as $row){
            if($row->getIsAnalyse()){
                $datas[] = $row;
            }
        }
        if(empty($datas)){
            return false;
        }else {
            return $datas[0];
        }
    }

    /**
     * Get nbTotal
     * @return int
     */
    public function Nb(){
        return count($this->moleculesEchantillon);
    }
}
