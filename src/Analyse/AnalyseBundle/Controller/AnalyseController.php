<?php

namespace Analyse\AnalyseBundle\Controller;

use Analyse\AnalyseBundle\Form\SearchEchantillonType;
use Doctrine\Common\Collections\ArrayCollection;
use Echantillon\EchantillonBundle\Entity\Echantillon;
use PDO;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use function Couchbase\defaultDecoder;

class AnalyseController extends Controller
{
    public function indexAction()
    {
        ini_set('memory_limit', '2048M');
        $em = $this->getDoctrine()->getManager();
        $echantillons = $em->getRepository("EchantillonBundle:Echantillon")->getNotYetAnalyse();
        return $this->render("AnalyseBundle:Default:resultat_index.html.twig", [
            "echantillons" => $echantillons
        ]);
    }

    public function addAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $echantillon = $em->getRepository("EchantillonBundle:Echantillon")->find($id);
        $test = [];
        if($request->getMethod() == "POST"){
            $batchSize = 20;
            $count = 1;
            if ($request->request->get("molecule")){
                ini_set('max_input_vars', 10000000000000000);
                ini_set('suhosin.post.max_vars', 10000000000000000);
                ini_set('suhosin.request.max_vars', 10000000000000000);
                foreach ($request->request->get("molecule") as $idMolecule => $moleculeData){
                    if ( (!empty($moleculeData["Resultat"]) && $moleculeData["Resultat"] != "ND") || isset($moleculeData["abs"]) ){

                        $moleculeEchantillon = $em->getRepository("EchantillonBundle:MoleculesEchantillon")->find($idMolecule);
                        $moleculeEchantillon->setIsAnalyse(true);
                        $moleculeEchantillon->setDateAnalyse(new \DateTime());

                        if (isset($moleculeData["tauxRecup"]) && !empty($moleculeData["tauxRecup"])) {
                            $moleculeEchantillon->setTauxRecup($moleculeData["tauxRecup"]);
                        }

                        if (isset($moleculeData["abs"])) {
                            $moleculeEchantillon->setResulat("ND");
                        } else {
                            $moleculeEchantillon->setResulat($moleculeData["Resultat"]);
                        }

                        if (isset($moleculeData["commentaire"]) && $moleculeData["commentaire"] != "") {
                            $moleculeEchantillon->setCommentaire(trim($moleculeData["commentaire"]));
                        }

                        if (($count % $batchSize) === 0) {
                            $em->flush();
                            $em->clear();
                        }
                        $count++;
                    }
                }
                $em->flush();
                $em->clear();
            }
            $this->addFlash("success", "Les résultats ont bien été enregistrés.");
        }

        /*$echantillonMolecules = $em->getRepository("EchantillonBundle:MoleculesEchantillon")->findBy([
            "echantillon" => $id,
            "isAnalyse" => false
        ]);*/

        $familles = $em->getRepository("EchantillonBundle:MoleculesEchantillon")->getFamillesForAnalyse($id);


        foreach ($familles as $famille){
            $molecules  = $em->getRepository("EchantillonBundle:MoleculesEchantillon")->getMoleculesInFamilleForAnalyse($id, $famille['nom']);
            $row["famille"] = $famille["nom"];
            $row["echantillons"] = $molecules;
            $test[] = $row;
        }

        /*$arrayFamille = ["GC", "LC", "HAP"];
        $collection = new ArrayCollection();*/

        /*foreach ($echantillonMolecules as $molecule){
            if(in_array($molecule->getMolecules()->getFamille()->getNom(), $arrayFamille)){
                if(!$this->inArrayMolecule($molecule->getEchantillon()->getReferenceLacapa(), $molecule->getMolecules()->getFamille()->getNom() ,$collection)){

                    $data["analyse"]        = "Toutes";

                    $data["REF"]            = $molecule->getEchantillon()->getReferenceLacapa();
                    $data["client"]         = $molecule->getEchantillon()->getClient()->getNom();
                    $data["echantillon"]    = $molecule->getEchantillon()->getNom();
                    $data["famille"]        = $molecule->getMolecules()->getFamille()->getNom();
                    $data["remarque"]       = $molecule->getEchantillon()->getCommentaire();


                    $collection->add($data);
                }
            }else {
                $data["REF"]            = $molecule->getEchantillon()->getReferenceLacapa();
                $data["id"]             = $molecule->getId();
                $data["analyse"]        = $molecule->getMolecules()->getNom();
                $data["famille"]        = $molecule->getMolecules()->getFamille()->getNom();

                $collection->add($data);
            }
        }*/

        return $this->render("@Analyse/Default/resultat_add.html.twig", [
            "moleculesEchantillon" => $test,
            "echantillon"   => $echantillon
        ]);
    }

    public function familleAction(Request $request, $id, $nom)
    {
        $em = $this->getDoctrine()->getManager();
        $echantillon = $em->getRepository("EchantillonBundle:Echantillon")->find($id);
        $moleculesEchantillon = $em->getRepository("EchantillonBundle:MoleculesEchantillon")->getByFamilleAndEchantillon($id, $nom);

        if($request->getMethod() == "POST"){
            if($request->request->get("absence") == "on"){
                foreach ($moleculesEchantillon as $row){
                    $row->setIsAnalyse(1);
                    $row->setResulat("ND");
                    $row->setDateAnalyse(new \DateTime());
                }
            }else {
                $datas = $request->request->get("analyse");
                foreach ($datas as $key => $value){
                    if($value["resultat"] != ""){

                        $moleculeEchantillon = $em->getRepository("EchantillonBundle:MoleculesEchantillon")->find($key);
                        $moleculeEchantillon->setIsAnalyse(1);
                        $moleculeEchantillon->setResulat($value["resultat"]);
                        $moleculeEchantillon->setDateAnalyse(new \DateTime());

                        if($value["commentaire"] != ""){
                            $moleculeEchantillon->setCommentaire($value["commentaire"]);
                        }
                    }
                }
            }
            $em->flush();
            $this->addFlash("success", "Les résultats de la famille ".$nom." pour l'échantillon ". $echantillon->getReferenceLacapa()." ont bien été enregistrés.");
            return $this->redirectToRoute("analyse_add", ["id" => $id]);
        }

        return $this->render("AnalyseBundle:Default:resultat_famille_add.html.twig", [
            "moleculesEchantillon"  => $moleculesEchantillon,
            "echantillon"           => $echantillon,
            "nom"                   => $nom
        ]);
    }

    public function listDoneAction()
    {
        ini_set('memory_limit', '2048M');
        $em = $this->getDoctrine()->getManager();
        $echantillons = $em->getRepository("EchantillonBundle:Echantillon")->getAnalyse();

        return $this->render("AnalyseBundle:Default:resultat_list_done.html.twig", [
            "echantillons" => $echantillons
        ]);
    }

    public function ficheAction(Request $request, $ref)
    {
        $em = $this->getDoctrine()->getManager();

        if($request->getMethod() == "POST"){
            $datas = $request->request->get("molecule");
            foreach ($datas as $key => $value){
                $moleculeEchantillon = $em->getRepository("EchantillonBundle:MoleculesEchantillon")->find($key);
                if(isset($value["raz"]) && $value["raz"] == "on"){
                    $moleculeEchantillon->setIsAnalyse(false);
                    $moleculeEchantillon->setResulat("");
                    $moleculeEchantillon->setCommentaire("");
                    $moleculeEchantillon->setInPaillasse(0);
                }else {
                    if($value["resultat"] != $moleculeEchantillon->getResulat()){
                        $moleculeEchantillon->setResulat($value["resultat"]);
                    }
                    if(isset($value["tauxRecup"]) && $value["tauxRecup"] != $moleculeEchantillon->getTauxRecup()){
                        $moleculeEchantillon->setTauxRecup($value["tauxRecup"]);
                    }
                    if (isset($value['commentaire']) && $value['commentaire'] != $moleculeEchantillon->getCommentaire()){
                        $moleculeEchantillon->setCommentaire($value['commentaire']);
                    }
                }
            }
            $em->flush();
            $this->addFlash("success", "Opération effectuée.");
            return $this->redirectToRoute("analyse_get_result", ['ref' => $ref]);
        }

        $echantillon = $em->getRepository("EchantillonBundle:Echantillon")->getByRefLacapa($ref);

        $familles = $em->getRepository("EchantillonBundle:MoleculesEchantillon")->getFamillesResults($echantillon->getId(), "analyse");

        $test = [];

        foreach ($familles as $famille){
            $molecules = $em->getRepository("EchantillonBundle:MoleculesEchantillon")->getMoleculesByFamille($famille["nom"], $echantillon->getId(), "analyse");
            $row["famille"] = $famille["nom"];
            $row["echantillons"] = $molecules;
            $test[] = $row;
        }

        return $this->render("AnalyseBundle:Default:resultat_list_molecule_display_done.html.twig", [
            "moleculesEchantillon" => $test,
            "echantillon"          => $ref
        ]);
    }

    public function searchAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(SearchEchantillonType::class);
        $echantillons = array();
        $search = [];
        $form->handleRequest($request);
        if($form->isValid() && $form->isSubmitted()){
            $datas = $form->getData();
            $echantillons = $em->getRepository("EchantillonBundle:Echantillon")->search($datas);

            /** @var Echantillon $echantillon */
            foreach ($echantillons as $echantillon){
                $row = [];
                $row['echantillon']   = $echantillon;
                $row['dates']         = $em->getRepository("EchantillonBundle:Echantillon")->getPaillasses($echantillon);
                $search[] = $row;
            }
        }
        return $this->render("AnalyseBundle:Default:resulat_search.html.twig", [
            "form" => $form->createView(),
            "echantillons" => $search
        ]);
    }

    private function inArrayMolecule(string $ref,string $familleSearch , $array){
        $res = false;
        foreach ($array as $row){
            if($row["REF"] == $ref && $row["famille"] == $familleSearch){
                $res = true;
            }
        }
        return $res;
        /*$datas = array();
        foreach ($array as $row){
            $datas[] = $row["REF"];
        }
        if(in_array($ref, $datas)){
            return true;
        }else {
            return false;
        }*/
    }

    /**
     * @param Int $echantillon
     * @return mixed
     */
    private function getPaillasse($echantillon) {
        /** @var PDO $conn */
        $conn = $this->getDoctrine()->getConnection();
        $sql = "SELECT Paillasse.* FROM Paillasse
JOIN paillasse_molecule pm on Paillasse.id = pm.paillasse_id
JOIN molecules_echantillon me on pm.molecule_echantillon_id = me.id
JOIN Echantillon E on me.echantillon_id = E.id
WHERE E.id = :echantillon AND Paillasse.actif = 1
GROUP BY Paillasse.id
ORDER BY Paillasse.dateCreation ASC
";
        $statement = $conn->prepare($sql);
        $statement->execute([
           'echantillon'    => $echantillon
        ]);
        return $statement->fetchAll(PDO::FETCH_ASSOC);

    }
}
