<?php
/**
 * Created by PhpStorm.
 * User: deshiloh
 * Date: 16/08/2017
 * Time: 15:10
 */

namespace Analyse\AnalyseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PaillasseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("values", CollectionType::class, [
                "entry_type" => MoleculePaillasseType::class,
                "entry_options" => [
                  "label" => false
                ],
                "label" => "Molécules à analyser",
                "allow_add" => true,
                "allow_delete" => true,
                "prototype" => true,
                "attr" => [
                    "class" => "mySelector"
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([

        ]);
    }
}