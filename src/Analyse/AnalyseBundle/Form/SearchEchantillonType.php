<?php
/**
 * Created by PhpStorm.
 * User: deshiloh
 * Date: 06/09/2017
 * Time: 14:21
 */

namespace Analyse\AnalyseBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class SearchEchantillonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("echantillon", TextType::class, [
                "required" => false,
                "label" => "Numéro Lacapa"
            ])
            ->add("NomEchantillon", TextType::class, [
                "required" => false,
                "label" => "Nom échantillon"
            ])
            ->add("ReferenceClient", TextType::class, [
                "required" => false,
                "label" => "Référence échantillon"
            ])
            ->add("Analyse", TextType::class, [
                "required"  => false
            ])
            ->add("NomClient", TextType::class, [
                "required" => false
            ])
            ->add("Presence", CheckboxType::class, [
                "label"     => "Présence de résultats",
                "required"  => false
            ])
            ->add('DateArriv', DateType::class, [
                'label' => "Date d'arrivée",
                'required'  => false,
                // 'data'  => new \DateTime(),
                'html5' => true,
                'widget'    => 'single_text',
            ])
            ->add('Famille', TextType::class, [
                'label'     => 'Famille',
                'required'  => false
            ])
            ->add("submit", SubmitType::class, [
                "label" => "Rechercher",
                "attr" => [
                    "class" => "btn-primary"
                ]
            ])
        ;
    }
}