<?php

namespace Analyse\AnalyseBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ModeleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("Libelle", TextType::class,[

            ])
            ->add("Molecules", HiddenType::class, [
                "attr" => [
                    "class" => "molecules_datas"
                ]
            ])
            ->add("submit", SubmitType::class, [
                "label" => $options["submit_button_label"],
                "attr" => [
                    "class" => "btn-primary margin-submit"
                ]
            ])
        ;

        $builder->get("Molecules")->addModelTransformer(new CallbackTransformer(
            function ($moleculesAsArray){
                if(!empty($moleculesAsArray)){
                    return implode("|", $moleculesAsArray);
                }
            },
            function($moleculesAsString){
                return explode("|", $moleculesAsString);
            }
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
           "submit_button_label" => "Ajouter"
        ]);
    }
}