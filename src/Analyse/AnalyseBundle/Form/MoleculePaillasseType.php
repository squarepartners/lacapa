<?php
/**
 * Created by PhpStorm.
 * User: deshiloh
 * Date: 21/08/2017
 * Time: 15:34
 */

namespace Analyse\AnalyseBundle\Form;


use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class MoleculePaillasseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("molecules", EntityType::class, [
               "class" => "Echantillon\EchantillonBundle\Entity\MoleculesEchantillon",
                "expanded" => true,
                "choice_label" => function($moleculesEchantillon){
                    if($moleculesEchantillon->getEchantillon()->getUrgent()){
                        return " /!\ ".$moleculesEchantillon->getEchantillon()->getReferenceLacapa() ." ". $moleculesEchantillon->getMolecules()->getNom();
                    }else {
                        return $moleculesEchantillon->getEchantillon()->getReferenceLacapa() ." ". $moleculesEchantillon->getMolecules()->getNom();
                    }
                }
            ])
        ;
    }
}