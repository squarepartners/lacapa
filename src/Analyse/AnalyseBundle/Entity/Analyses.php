<?php

namespace Analyse\AnalyseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Analyses
 *
 * @ORM\Table(name="analyses")
 * @ORM\Entity(repositoryClass="Analyse\AnalyseBundle\Repository\AnalysesRepository")
 */
class Analyses
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Resultat", type="blob", nullable=true)
     */
    private $resultat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateCreation", type="datetime")
     */
    private $dateCreation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateModif", type="datetime")
     */
    private $dateModif;

    /**
     * @ORM\ManyToOne(targetEntity="Utilisateurs\UtilisateursBundle\Entity\Utilisateurs", inversedBy="analyses")
     */
    private $utilisateur;

    /**
     * @ORM\OneToOne(targetEntity="Molecules\MoleculesBundle\Entity\Molecules")
     */
    private $molecule;

    /**
     * @ORM\OneToOne(targetEntity="Echantillon\EchantillonBundle\Entity\Echantillon")
     */
    private $echantillon;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set resultat
     *
     * @param string $resultat
     *
     * @return Analyses
     */
    public function setResultat($resultat)
    {
        $this->resultat = $resultat;

        return $this;
    }

    /**
     * Get resultat
     *
     * @return string
     */
    public function getResultat()
    {
        return $this->resultat;
    }

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     *
     * @return Analyses
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set dateModif
     *
     * @param \DateTime $dateModif
     *
     * @return Analyses
     */
    public function setDateModif($dateModif)
    {
        $this->dateModif = $dateModif;

        return $this;
    }

    /**
     * Get dateModif
     *
     * @return \DateTime
     */
    public function getDateModif()
    {
        return $this->dateModif;
    }

    /**
     * Set utilisateur
     *
     * @param \Utilisateurs\UtilisateursBundle\Entity\Utilisateurs $utilisateur
     *
     * @return Analyses
     */
    public function setUtilisateur(\Utilisateurs\UtilisateursBundle\Entity\Utilisateurs $utilisateur = null)
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    /**
     * Get utilisateur
     *
     * @return \Utilisateurs\UtilisateursBundle\Entity\Utilisateurs
     */
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }

    /**
     * Set molecule
     *
     * @param \Molecules\MoleculesBundle\Entity\Molecules $molecule
     *
     * @return Analyses
     */
    public function setMolecule(\Molecules\MoleculesBundle\Entity\Molecules $molecule = null)
    {
        $this->molecule = $molecule;

        return $this;
    }

    /**
     * Get molecule
     *
     * @return \Molecules\MoleculesBundle\Entity\Molecules
     */
    public function getMolecule()
    {
        return $this->molecule;
    }

    /**
     * Set echantillon
     *
     * @param \Echantillon\EchantillonBundle\Entity\Echantillon $echantillon
     *
     * @return Analyses
     */
    public function setEchantillon(\Echantillon\EchantillonBundle\Entity\Echantillon $echantillon = null)
    {
        $this->echantillon = $echantillon;

        return $this;
    }

    /**
     * Get echantillon
     *
     * @return \Echantillon\EchantillonBundle\Entity\Echantillon
     */
    public function getEchantillon()
    {
        return $this->echantillon;
    }

    /**
     * Set methode
     *
     * @param \Molecules\MoleculesBundle\Entity\Methodes $methode
     *
     * @return Analyses
     */
    public function setMethode(\Molecules\MoleculesBundle\Entity\Methodes $methode = null)
    {
        $this->methode = $methode;

        return $this;
    }

    /**
     * Get methode
     *
     * @return \Molecules\MoleculesBundle\Entity\Methodes
     */
    public function getMethode()
    {
        return $this->methode;
    }
}
