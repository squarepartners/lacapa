<?php

namespace Analyse\AnalyseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ModeleAnalyse
 *
 * @ORM\Table(name="modele_analyse")
 * @ORM\Entity(repositoryClass="Analyse\AnalyseBundle\Repository\ModeleAnalyseRepository")
 */
class ModeleAnalyse
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Libelle", type="string", length=255, nullable=true)
     */
    private $libelle;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateCreation", type="datetime")
     */
    private $dateCreation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateModif", type="datetime")
     */
    private $dateModif;

    /**
     * @var array
     * @ORM\Column(name="Molecules", type="array")
     */
    private $molecules;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     *
     * @return ModeleAnalyse
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     *
     * @return ModeleAnalyse
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set dateModif
     *
     * @param \DateTime $dateModif
     *
     * @return ModeleAnalyse
     */
    public function setDateModif($dateModif)
    {
        $this->dateModif = $dateModif;

        return $this;
    }

    /**
     * Get dateModif
     *
     * @return \DateTime
     */
    public function getDateModif()
    {
        return $this->dateModif;
    }

    /**
     * Set molecules
     *
     * @param array $molecules
     *
     * @return ModeleAnalyse
     */
    public function setMolecules($molecules)
    {
        $this->molecules = $molecules;

        return $this;
    }

    /**
     * Get molecules
     *
     * @return array
     */
    public function getMolecules()
    {
        return $this->molecules;
    }
}
