$(document).ready(function () {
    $.sessionTimeout({
        keepAliveUrl: false,
        logoutUrl: false,
        redirUrl: '/logout',
        warnAfter: (20 * 60) * 1000,
        redirAfter: ((20 * 60) * 1000) + 10000,
        countdownMessage: 'Déconnexion dans {timer} secondes.',
        countdownBar: true
    });
});